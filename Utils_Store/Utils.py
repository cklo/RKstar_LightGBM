# Import Libraries
import os
import pandas as pd
import numpy as np
import h5py
import re
import sys
sys.path.append("..")
sys.path.append("../2BDT_w_extra_features/")
#sys.path.append(os.getcwd()+'/RK_Star_Analysis/RKStar_Analysis')
#sys.path.append(os.getcwd()+'..')
#print(os.getcwd()+'/RK_Star_Analysis/RKStar_Analysis')
#sys.path.append("./RK_Star_Analysis/RKStar_Analysis/Verstack_cluster_safe")
import Verstack_cluster_safe.LGBMTuner as verstack
import optuna
import lightgbm
import sklearn.preprocessing
#from Verstack_cluster_safe.LGBMTuner import LGBMTuner as verstack
from sklearn import decomposition
from sklearn import manifold
from sklearn.decomposition import KernelPCA
import toml

# Loading config -------------------------------------------------------------------------------------------------------
config = toml.load('config.toml')
channel = config['channel']['channel']
#-----------------------------------------------------------------------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

def selection_cuts():
    if channel == "electron":
        selection_main   = '((info_diElectron_gsf_id_isOK == 1) & (positron_charge * electron_charge < 0) & (electron_iso_c40 < 100000) & (electron_iso_c40 < 100000))'
        B_mass_Kstar_mass_closer__string   =  '((abs(diMeson_Kpi_mass-891.66)<abs(diMeson_piK_mass-891.66))*B_mass+(abs(diMeson_Kpi_mass-891.66)>=abs(diMeson_piK_mass-891.66))*Bbar_mass)'
        selection_nn1_sb = f"( ((({B_mass_Kstar_mass_closer__string}) < 4000) | ( ({B_mass_Kstar_mass_closer__string}) > 5700) ) & (B_chi2 < 20))"
        selection_nn1_sr = f"( (({B_mass_Kstar_mass_closer__string}) > 4000) & (({B_mass_Kstar_mass_closer__string}) < 5700) & (B_chi2 < 20))"
        selection_nn2_sb = '(trackPlus_charge * trackMinus_charge > 0)'
        selection_nn2_sr = '(trackPlus_charge * trackMinus_charge < 0)'
        selection_q2low  = '( (diElectron_mass*diElectron_mass > 1.1e6) & (diElectron_mass*diElectron_mass <= 6e6) )'
        selection_q2high = '( (diElectron_mass*diElectron_mass > 6e6) & (diElectron_mass*diElectron_mass <= 11e6) )'
        selection_q2lcr  = '(diElectron_mass*diElectron_mass <= 1.1e6)'
        selection_q2hcr  = '(diElectron_mass*diElectron_mass > 11e6)'

    elif channel == "muon":
        selection_main   = '((antimuon_charge * muon_charge < 0) & (muon_iso_c40 < 100000) & (antimuon_iso_c40 < 100000))'
        B_mass_Kstar_mass_closer__string   =  '((abs(diMeson_Kpi_mass-891.66)<abs(diMeson_piK_mass-891.66))*B_mass+(abs(diMeson_Kpi_mass-891.66)>=abs(diMeson_piK_mass-891.66))*Bbar_mass)'
        selection_nn1_sb = f"( (((4700 < {B_mass_Kstar_mass_closer__string}) < 5000) | ( (5700 < {B_mass_Kstar_mass_closer__string}) < 6000) ) & (B_chi2 < 20))"
        selection_nn1_sr = f'( (({B_mass_Kstar_mass_closer__string}) > 5000) & (({B_mass_Kstar_mass_closer__string}) < 5700) & (B_chi2 < 20) )'
        selection_nn2_sb = '(trackPlus_charge * trackMinus_charge > 0)'
        selection_nn2_sr = '(trackPlus_charge * trackMinus_charge < 0)'
        selection_q2low  = '( (diMuon_mass*diMuon_mass > 1.1e6) & (diMuon_mass*diMuon_mass <= 6e6) )'
        selection_q2high = '( (diMuon_mass*diMuon_mass > 6e6) & (diMuon_mass*diMuon_mass <= 11e6) )'
        selection_q2lcr  = '(diMuon_mass*diMuon_mass <= 1.1e6)'
        selection_q2hcr  = '(diMuon_mass*diMuon_mass > 11e6)'

    usr_all =     f"({selection_main} &  {selection_nn1_sr} & {selection_nn2_sr})"
    nn1trb_all = f"({selection_main} &  {selection_nn1_sb} & {selection_nn2_sr})"
    nn2trb_all = f"({selection_main} &  {selection_nn1_sr} & {selection_nn2_sb})"

    usr_lowq2 =     f"({selection_main} & {selection_q2low} &  {selection_nn1_sr} & {selection_nn2_sr})"
    nn1trb_lowq2 = f"({selection_main} & {selection_q2low} &  {selection_nn1_sb} & {selection_nn2_sr})"
    nn2trb_lowq2 = f"({selection_main} & {selection_q2low} & {selection_nn1_sr} & {selection_nn2_sb})"

    usr_highq2 =     f"({selection_main} & {selection_q2high} &  {selection_nn1_sr} & {selection_nn2_sr})"
    nn1trb_highq2 = f"({selection_main} & {selection_q2high} &  {selection_nn1_sb} & {selection_nn2_sr})"
    nn2trb_highq2 = f"({selection_main} & {selection_q2high} & {selection_nn1_sr} & {selection_nn2_sb})"

    Selections = {
        "usr_all"       :  usr_all,
        "nn1trb_all"    :  nn1trb_all,
        "nn2trb_all"    :  nn2trb_all,
        "usr_lowq2"     :  usr_lowq2,
        "nn1trb_lowq2"  :  nn1trb_lowq2,
        "nn2trb_lowq2"  :  nn2trb_lowq2,
        "usr_highq2"    :  usr_highq2,
        "nn1trb_highq2" :  nn1trb_highq2,
        "nn2trb_highq2" :  nn2trb_highq2,
        "main"          :  selection_main,
        }
    return Selections


class HDF5_IO():
    __module__ = 'Storage.Global'

    def __init__(self,path:str):
        """ Easy write/read HDF5 files

        Args:
            path (str): Path for the HDF5 file which is serving as a project storage file
        """
        assert path.split('.')[-1] in ["h5","hdf5"], "file format is not a HDF5 format."
        self.path:str = path

        # Run initializing functions
        self.__create_project_HDF5_file()

    
    def __create_project_HDF5_file(self):
        """ If HDF5 does not exist at 'path' location, create it.
        """
        if os.path.exists(self.path) == True:
            pass
        else:
            with h5py.File(f"{self.path}", "w-") as f:
                #f.create_group("raw_data")
                f.close()


    def move_to_new(self,old_path):
        assert old_path.split('.')[-1] in ["h5","hdf5"], "file format is not a HDF5 format"
        with pd.HDFStore(old_path, 'r') as hdf:
            hdf5_keys = hdf.keys()
            hdf.close()

        dict = {}
        for key in list(hdf5_keys):
            print(re.sub('\W+','', key))
            #dict[re.sub('\W+','', key)] = pd.read_hdf(old_path,key)


    def print_structure(self):
        with pd.HDFStore(f"{self.path}", "r") as f:
            print(f.info())
            f.close()

    #def write_pd(self,key:str,df: (pd.core.frame.DataFrame | pd.core.series.Series),overwrite=False):
    def write_pd(self, key: str, df: (pd.core.frame.DataFrame), overwrite=False):
        """ Write to file

        Args:
            key (str): key for the file. Also acts as a path in the HDF5 file.
            df (pd.core.frame.DataFrame): The Pandas DataFrame to be stored.
            overwrite (bool, optional): If True, overwrite what is a the 'key' location. Defaults to False.
        """
        if key[0] != "/":
            key = "/"+key
        with pd.HDFStore(f"{self.path}", "a") as f:
            if (key in f.keys()) & (overwrite == True):
                f.put(key=key,value=df,format='fixed') # to add a dataframe to the hdf file
            elif (key not in f.keys()):
                f.put(key=key,value=df,format='fixed')
            f.close()

    def read_pd(self,key:str):
        """ Reads Pandas DataFrames

        Args:
            key (str): key for the file. Also acts as a path in the HDF5 file.
        Returns:
            pd.DataFrame: Output DataFrame with key: 'key'
        """
        if key[0] != "/":
            key = "/"+key
        with pd.HDFStore(f"{self.path}", "a") as f:
            data=f.get(key)
            f.close()
        return data

    def del_dataset(self,key:str):
        """ Deletes dataset at location: 'key'

        Args:
            key (str): key for the file. Also acts as a path in the HDF5 file.
        """
        if key[0] != "/":
            key = "/"+key
        with pd.HDFStore(f"{self.path}", "a") as f:
            del f[key]
            f.close()

    def save_lgbm(self,key:str,tuner,feature_names:list,overwrite=True):

        tuner.fitted_model.save_model('intermediate_model_file.txt')
        with open('intermediate_model_file.txt','r') as f:
            lines = [line.split('\n')[0] for line in f]
            f.close()
        df=pd.DataFrame(lines)
        os.remove("intermediate_model_file.txt")

        for i in range(len(df)):
            if df.iloc[i].values[0].split('=')[0] == 'feature_names':
                df.iloc[i].values[0] = df.iloc[i].values[0].split('=')[0] + '='+ ' '.join(feature_names)
                break

        if key[0] != "/":
            key = "/"+key

        with pd.HDFStore(f"{self.path}", "a") as f:
            if ((key in f.keys()) & (overwrite == True)) or (key not in f.keys()):
                f.put(key=key,value=df,format='fixed') # to add a dataframe to the hdf file
            f.close()

    def load_lgbm(self,key:str):
        if key[0] != "/":
            key = "/"+key
        with pd.HDFStore(f"{self.path}", "a") as f:
            data=f.get(key)
            f.close()

        with open('intermediate_model_file.txt', 'w') as f:
            for line in data.values:
                f.write(line[0])
                f.write('\n')
            f.close()

        booster=lightgbm.Booster(model_file='intermediate_model_file.txt')
        os.remove("intermediate_model_file.txt")


        return booster



# ________________________________________________________________________
""" 
Fit scaler and save to global storage
"""
# ________________________________________________________________________


def fit_scaler_to_train(Storage,X,scaler_type:str = 'RobustScaler', root_tag:str='LGBM_all'):
    """Fit scaler and save to global storage

    Args:
        Storage (Storage.Global.HDF5_IO): Global HDF5 storage
        scaler_type (str): Which scaler to be used. Defaults to 'RobustScaler'.
        root_tag (str, optional): key to the HDF5 file where original data is stored. Defaults to 'LGBM_all'.
    """

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"
    np.random.seed(42)
    transformer = eval('sklearn.preprocessing.'+ scaler_type +'()')
    transformer = transformer.fit(X)
    scaler_pd = pd.DataFrame.from_dict(transformer.__dict__, orient='index',columns=[str(type(transformer)).strip("<>'").split('.')[-1]])
    Storage.write_pd(key=f"{root_tag}/scaler", df = scaler_pd, overwrite=True)

def pd_scale(Storage, X, root_tag:str = 'LGBM_all',target_tag:str = 'X_valid'):
    """Transform target_tag-data to scaler which is saved at root_tag

    Args:
        Storage (Storage.Global.HDF5_IO): Global HDF5 storage
        root_tag (str, optional): key to the HDF5 file where original data is stored. Defaults to 'LGBM_all'.
        target_tag (str, optional): key to the HDF5 file where target data is stored. Defaults to 'X_valid'.
    """
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"
    np.random.seed(42)

    scaler_pd = Storage.read_pd(key=f"{root_tag}/scaler")
    scaler_type = list(scaler_pd.columns)[0]
    transformer = eval('sklearn.preprocessing.'+scaler_type+'()')
    
    assert str(type(transformer)).strip("<>'").split('.')[-1] == scaler_type, "Scaler type does not match with loaded scaler"

    transformer.__dict__= scaler_pd.to_dict(orient='dict')[scaler_type]
    target_pd = X
    scaled_target= pd.DataFrame(transformer.transform(target_pd),columns=target_pd.columns)
    return scaled_target
    Storage.write_pd(key=f"{root_tag}/{target_tag}_scaled", df = scaled_target, overwrite=True)


def pd_inv_scale(Storage, X, root_tag:str = 'LGBM_all'):
    """Inverse transforms dataframe

    Args:
        Storage (Storage.Global.HDF5_IO): Global HDF5 storage
        X (pd.DataFrame): Input
        root_tag (str, optional): key to the HDF5 file where original data is stored. Defaults to 'LGBM_all'.
    Returns:
        _type_: pd.DataFrame: inverse transformed of X
    """

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"
    np.random.seed(42)

    scaler_pd = Storage.read_pd(key=f"{root_tag}/scaler")
    scaler_type = list(scaler_pd.columns)[0]
    transformer = eval('sklearn.preprocessing.'+scaler_type+'()')
    
    assert str(type(transformer)).strip("<>'").split('.')[-1] == scaler_type, "Scaler type does not match with loaded scaler"

    transformer.__dict__= scaler_pd.to_dict(orient='dict')[scaler_type]
    target_pd = X
    inv_scaled_target= pd.DataFrame(transformer.inverse_transform(target_pd),columns=target_pd.columns)

    return inv_scaled_target


# ________________________________________________________________________
""" 
Fit scaler and save to global storage
"""
# ________________________________________________________________________
from iminuit.util import make_func_code
from iminuit import describe #, Minuit,

class Chi2Regression:  # override the class with a better one
    """ Function developed by Troels C Petersen (https://github.com/AppliedStatisticsNBI/AppStat2021/tree/main/External_Functions)
    and modified for needs: by Daniel Hans Munk
    """
        
    def __init__(self, f, x, y, sy=None, weights=None, bound=None):
        
        if bound is not None:
            x = np.array(x)
            y = np.array(y)
            sy = np.array(sy)
            mask = (x >= bound[0]) & (x <= bound[1])
            x  = x[mask]
            y  = y[mask]
            sy = sy[mask]

        self.f = f  # model predicts y for given x
        self.x = np.array(x)
        self.y = np.array(y)
        
        self.sy = self._set_var_if_None(sy, self.x)
        self.weights = self._set_var_if_None(weights, self.x)
        self.func_code = make_func_code(describe(self.f)[1:])

    
    def _set_var_if_None(self,var, x):
        if var is not None:
            return np.array(var)
        else: 
            return np.ones_like(x)

    def _compute_f(self,f, x, *par):
        try:
            return f(x, *par)
        except ValueError:
            return np.array([f(xi, *par) for xi in x])

    def __call__(self, *par):  # par are a variable number of model parameters
        
        # compute the function value
        f = self._compute_f(self.f, self.x, *par)
        
        # compute the chi2-value
        chi2 = np.sum(self.weights*(self.y - f)**2/self.sy**2)
        
        return chi2

def values_to_string(values, decimals):
    """ Function developed by Troels C Petersen (https://github.com/AppliedStatisticsNBI/AppStat2021/tree/main/External_Functions)
    and modified for needs: by Daniel Hans Munk
  
    Loops over all elements of 'values' and returns list of strings
    with proper formating according to the function 'format_value'. 
    """
    

    def format_value(value, decimals):
        """ 
        Checks the type of a variable and formats it accordingly.
        Floats has 'decimals' number of decimals.
        """
        if isinstance(value, (float, np.float)):
            return f'{value:.{decimals}f}'
        elif isinstance(value, (int, np.integer)):
            return f'{value:d}'
        else:
            return f'{value}'

    res = []
    for value in values:
        if isinstance(value, list):
            tmp = [format_value(val, decimals) for val in value]
            res.append(f'{tmp[0]} +/- {tmp[1]}')
        else:
            res.append(format_value(value, decimals))
    return res

def nice_string_output(d, extra_spacing=5, decimals=3):
    """ Function developed by Troels C Petersen (https://github.com/AppliedStatisticsNBI/AppStat2021/tree/main/External_Functions)
    and modified for needs: by Daniel Hans Munk
    
    Takes a dictionary d consisting of names and values to be properly formatted.
    Makes sure that the distance between the names and the values in the printed
    output has a minimum distance of 'extra_spacing'. One can change the number
    of decimals using the 'decimals' keyword.  
    """
    def len_of_longest_string(s):
        """ Returns the length of the longest string in a list of strings """
        return len(max(s, key=len))
    
    names = d.keys()
    max_names = len_of_longest_string(names)
    
    values = values_to_string(d.values(), decimals=decimals)
    max_values = len_of_longest_string(values)
    
    string = ""
    for name, value in zip(names, values):
        spacing = extra_spacing + max_values + max_names - len(name) - 1 
        string += "{name:s} {value:>{spacing}} \n".format(name=name, value=value, spacing=spacing)
    return string[:-2]


def add_text_to_ax(x_coord, y_coord, string, ax, fontsize=12, color='k',border=True):
    """ Function developed by Troels C Petersen (https://github.com/AppliedStatisticsNBI/AppStat2021/tree/main/External_Functions)
    and modified for needs: by Daniel Hans Munk
    
    Shortcut to add text to an ax with proper font. Relative coords."""
    if border==True:
        ax.text(x_coord, y_coord, string, family='stixgeneral', fontsize=fontsize,
                transform=ax.transAxes, verticalalignment='top', color=color,bbox=dict(facecolor='white',edgecolor='black',boxstyle="round,pad=0.3,rounding_size=0.4",alpha=0.85))
    else:
        ax.text(x_coord, y_coord, string, family='stixgeneral', fontsize=fontsize,
                transform=ax.transAxes, verticalalignment='top', color=color)                    
    return None


import inspect
class iminut_value_initializer():
    """ Initialize object with fit-function before iminuit is called
    then use init_values to set initial values along with names when iminuit object is created.
    migrad is called, limits_and_fixed can be called to fix and set limits on variables.
    """
    def __init__(self,input_params,fit_function):
        self.params = pd.DataFrame(input_params).T
        func_params = list(inspect.signature(fit_function).parameters)

        assert all([elem in func_params for elem in list(self.params.index)]) == True, 'Error'

    def init_values(self):        
        init_values=tuple(self.params[0].values)
        return init_values
    
    def names(self):
        names=tuple(self.params.index.values)
        return names
    
    def limits_and_fixed(self,minuit_object):
        for i in range(len(self.params)):
            minuit_object.limits[self.params.index[i]] = (self.params[1][i],self.params[2][i])
            minuit_object.fixed[self.params.index[i]] = self.params[3][i]


# ________________________________________________________________________
""" 
Fit PCA and save to global storage
"""
# ________________________________________________________________________


def fit_PCA_to_train(Storage,X,n_components:int = 1,root_tag:str='LGBM_all'):
    """Fit scaler and save to global storage

    Args:
        Storage (Storage.Global.HDF5_IO): Global HDF5 storage
        scaler_type (str): Which scaler to be used. Defaults to 'RobustScaler'.
    """

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"
    
    np.random.seed(42)
    transformer = decomposition.PCA(n_components=n_components, random_state=42)
    transformer = transformer.fit(X)
    scaler_pd = pd.DataFrame.from_dict(transformer.__dict__, orient='index',columns=[n_components])
    Storage.write_pd(key=f"PCA/{root_tag}", df = scaler_pd, overwrite=True)

def PCA_transform(Storage, X, n_components:int = 1,root_tag:str = 'LGBM_all'):
    """Transform target_tag-data to scaler which is saved at root_tag

    Args:
        Storage (Storage.Global.HDF5_IO): Global HDF5 storage
        root_tag (str, optional): key to the HDF5 file where original data is stored. Defaults to 'LGBM_all'.
    """
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"
    np.random.seed(42)
    transformer = decomposition.PCA(n_components=n_components, random_state=42)

    scaler_pd = Storage.read_pd(key=f"PCA/{root_tag}")
    
    transformer.__dict__= scaler_pd.to_dict(orient='dict')[n_components]
    target_pd = X.copy()
    scaled_target= transformer.transform(target_pd)
    return scaled_target