Welcome to the LightGBM models for the RK* analysis. It was developed by Daniel Hans Munk, University of Copenhagen (2023).
Based on the Daniel branch, this branch is synchronized with both the electron and muon channels and can be run on lxplus platforms.

# [Documentation]
```bash
The documentation of the LightGBM model lives here: https://gitlab.cern.ch/dmunk/rkstar_masterthesis/-/tree/master/.
The documentation of the RK* Analysis lives here: https://gitlab.cern.ch/RKstar.
```

## Installation on lxplus 9 (to be adapted for other computer systems)
```bash
setupATLAS
lsetup "views LCG_102b_ATLAS_1 x86_64-centos9-gcc11-opt"
git clone https://:@gitlab.cern.ch:8443/cklo/RKstar_LightGBM.git
cd RKstar_LightGBM
python -m pip install --user -r requirements.txt
cd 2BDT_w_extra_features
```

## Tutorial
Example run commands:
1) Complete the config.toml file - You need to fill-in the channel, ML_config and directory information for the BDT model setup.

2) Start the BDT training and evaluate the BDT performance
```bash
python LightGBM_Training.py
python LightGBM_Prediction.py
```

3) Use trained BDT model for selection - You can modify the config.toml file for BDT_selection sections. Then you can try
```bash
python BDT_selection.py
```

4) Fit the B mass distribution using Roofit - You can modify the config.toml file for BDT_Roofit sections. Then you can try
```bash
python BDT_Roofit.py
```

## Working in Progress
1) Solving instability of hdf5 file (storing trained BDT model) during prediction or selection
2) Adding Extrapolation scripts + Roofit pdf model in electron side

## Working Environment
1) Please run on AFS platform instead of EOS platform (hdf5 already backup automatically during training)
2) Please rename your model (or delete your existing hdf5 file) in config.toml before every new training

# [Important analysis reference]
```bash
Recent BDT_Presentation by Daniel (https://indico.cern.ch/event/1280538/contributions/5379645/attachments/2635917/4560196/RK_star_update26042023.pdf)
```