# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import plot_utils as plotting
import toml
from sklearn import preprocessing, model_selection, metrics, decomposition
import seaborn as sns
import random

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit
import os

from sklearn.preprocessing import MinMaxScaler, StandardScaler, LabelBinarizer
#import verstack
import Verstack_cluster_safe.LGBMTuner as verstack
import optuna
import sklearn.inspection
from sklearn.base import BaseEstimator
random.seed(42)

mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

# Loading config -------------------------------------------------------------------------------------------------------
config = toml.load('config.toml')
channel = config['channel']['channel']
#-----------------------------------------------------------------------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

# Loading features -----------------------------------------------------------------------------------------------------
Storage = Utils.HDF5_IO(config['PATHS_{}'.format(channel)]['Storage'])
#safe_good_features = ['tracks_dEdx_diff', 'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
#safe_good_features = ['Lxy_significance_over_B_chi2','tracks_dEdx_diff','diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
safe_good_features = ['tracks_dEdx_diff', 'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]

Features_to_be_read = []
for k, v in config['features_{}'.format(channel)].items():
    Features_to_be_read.extend(v)
Features_to_be_read = Features_to_be_read + safe_good_features
features = config['features_{}'.format(channel)]['ML_features']
features = features + safe_good_features

# One-Component PCA (feature extraction, other possible directions: kernal PCA, tSNE, LDA, autoencoder ...) ------------

def FIT_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton),'{}_eta'.format(lepton),'{}_phi'.format(lepton),'{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton),'{}_eta'.format(antilepton),'{}_phi'.format(antilepton),'{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT','trackPlus_eta','trackPlus_phi','trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT','trackMinus_eta','trackMinus_phi','trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance",  "z0_significance",  "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double),"d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson= ["diMeson_angle_alpha_sym",  "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    Utils.fit_PCA_to_train(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_antilepton],n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackPlus],n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackMinus],n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dimeson],n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_quality],n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    Utils.fit_PCA_to_train(Storage,_Data[branch_track1_track2],n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton_dimeson],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dilepton],n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dimeson],n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

def TRANSFORM_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton), '{}_eta'.format(lepton), '{}_phi'.format(lepton), '{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton), '{}_eta'.format(antilepton), '{}_phi'.format(antilepton), '{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT', 'trackPlus_eta', 'trackPlus_phi', 'trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT', 'trackMinus_eta', 'trackMinus_phi', 'trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance", "z0_significance", "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double), "d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson = ["diMeson_angle_alpha_sym", "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    _Data['branch_{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    _Data['branch_{}'.format(antilepton)] = Utils.PCA_transform(Storage,_Data[branch_antilepton], n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    _Data['branch_trackPlus'] = Utils.PCA_transform(Storage,_Data[branch_trackPlus], n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    _Data['branch_trackMinus'] = Utils.PCA_transform(Storage,_Data[branch_trackMinus], n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    _Data['branch_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    _Data['branch_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_dimeson], n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    _Data['branch_b_meson_quality'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_quality], n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    _Data['branch_track1_track2'] = Utils.PCA_transform(Storage,_Data[branch_track1_track2], n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    _Data['branch_di{}_dimeson'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton_dimeson], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    _Data['branch_b_meson_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dilepton], n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    _Data['branch_b_meson_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dimeson], n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

    return _Data

# Prediction on All data -----------------------------------------------------------------------------------------------

import tables
import h5py

def get_B_mass_true(df):
    print('Adding True Mass Column..')
    df["B_mass_true"] = ((df["info_is_true_nonres_Bd"]) | (df["info_is_true_res_Bd"])) * df["B_mass"] + ((df["info_is_true_nonres_BdBar"]) | (df["info_is_true_res_BdBar"])) * df["Bbar_mass"]
    return df


def predict_double(Storage, X_target=None):
    print('Adding Predictions..')
    X = X_target.copy()

    # ------- BDT1 ----------
    
    root_tag = 'BDT1'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag=root_tag)
    X_scaled = X_scaled[feat_idx]

    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:, 2], BDT_scores[:, 1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)

    # ------- BDT2 ----------
    root_tag = 'BDT2'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag=root_tag)
    X_scaled = X_scaled[feat_idx]
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:, 2], BDT_scores[:, 1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)
    X_target['P_BDT_max'] = np.max([X_target.P_BDT1_sum, X_target.P_BDT2_sum], axis=0)

    return X_target


def load_multiple_feather_files(file_list: list, selection: str, N_events: int, columns_to_read: list):
    print('Starts loading files..')
    files = []
    List_uniques_list = []

    for file_path in file_list:
        files.append(pd.read_feather(path=file_path, columns=columns_to_read, use_threads=True, storage_options=None))
    print('Done loading..')

    if selection == "None":
        for i in range(len(files)):
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]
    else:
        for i in range(len(files)):
            files[i] = files[i].query(selection)
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]

    print(f"Number of uniques: {[len(i) for i in List_uniques_list]}, total = {sum([len(i) for i in List_uniques_list])}")
    print(f"Length of files: {[len(i) for i in files]}, total = {sum([len(i) for i in files])}")

    df_files = pd.concat(files)
    df_files_w_true_mass = get_B_mass_true(df_files)
    df_files_w_true_mass_w_predictions = predict_double(Storage, df_files_w_true_mass)
    print('Done..')
    print()

    return df_files_w_true_mass_w_predictions

if channel == "electron":
    files_dictionary = {
        'MC_signal': [os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_03.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_04.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300591_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300591_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300592_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300592_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300593_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300593_part_02.ftr')],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if config['DATA_{}'.format(channel)]['selection'] in x])
    }

elif channel == "muon":
    files_dictionary = {
        'MC_signal': [os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300700_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300700_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300701_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300701_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300702_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300703_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300703_part_02.ftr')],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if config['DATA_{}'.format(channel)]['selection'] in x])
    }

MC_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = Utils.selection_cuts()['usr_highq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

MC_USR_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = Utils.selection_cuts()['usr_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

NN1trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'],
    selection = Utils.selection_cuts()['nn1trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

NN2trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'],
    selection = Utils.selection_cuts()['nn2trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)


Data_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'],
    selection = Utils.selection_cuts()['usr_highq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

def BDT1_2Dresponse_in_USR():
    P = plotting.TwoD_response_curves(input_data=MC_USR_lowq2, threshold=0.4, root_tag='BDT1')

    fig = plt.figure(figsize=(9, 3.5), dpi=300)
    fig.suptitle('GBDT1 2D-Response in: $\{\mathrm{SR}^{\mathrm{MC}}\}$, $q_{low}^2$ | Global/Local Selection Rules Applied')
    P.plot_Bd(fig=fig, placemenent=121)
    P.plot_BdBar(fig=fig, placemenent=122)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_2D_response_USRlowq2", bbox_inches='tight', dpi=300)
    plt.show()

BDT1_2Dresponse_in_USR()

def BDT2_2Dresponse_in_USR():
    P = plotting.TwoD_response_curves(input_data=MC_USR_lowq2, threshold=0.4, root_tag='BDT2')

    fig = plt.figure(figsize=(9, 3.5), dpi=300)
    fig.suptitle('GBDT2 2D-Response in $\{\mathrm{SR}^{\mathrm{MC}}\}$, $q_{low}^2$ | Global/Local Selection Rules Applied')
    P.plot_Bd(fig=fig, placemenent=121)
    P.plot_BdBar(fig=fig, placemenent=122)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_2D_response_USRlowq2", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_2Dresponse_in_USR()

def BDT1_response_in_USR__NN1TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN1trb_lowq2], axis=0), threshold=0.1, root_tag='BDT1')

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.8, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT1 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_USR_NN1TRB_lowq2", bbox_inches='tight', dpi=300)

    plt.show()

BDT1_response_in_USR__NN1TRB()


def BDT2_response_in_USR__NN2TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN2trb_lowq2], axis=0), threshold=0.3,root_tag='BDT2')

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.8, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT2 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_USR_NN2TRB_lowq2", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_response_in_USR__NN2TRB()

def plot_res_vs_nonres_wrapper():
    P = plotting.plot_res_vs_nonres(input_data=pd.concat([MC_USR_lowq2,MC_USR_highq2],axis=0),threshold=0.65)

    fig = plt.figure(figsize=(8,3.5),dpi=300)
    #main_title = "GBDT's Response in $\{$SR $\cup$ SB2$\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"

    main_title = "GBDT's efficiency in $q^2_{{low}}$ and $q^2_{{high}}$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    title='GBDT1 Efficiency in $\{\mathrm{SR}^{\mathrm{MC}}\}$'
    P.Plot_2efficiency(fig=fig,placemenent=121,title=title,root_tag='BDT1')
    title='GBDT2 Efficiency in $\{\mathrm{SR}^{\mathrm{MC}}\}$'
    P.Plot_2efficiency(fig=fig,placemenent=122,title=title,root_tag='BDT2')
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT eff",bbox_inches = 'tight',dpi=300)

    plt.show()
plot_res_vs_nonres_wrapper()

def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12, 5), dpi=300)
    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + " GBDT1 scores for " + r"$\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$ " + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=121, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT1')

    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + r" GBDT2 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=122, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT2')
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "Mass_shape_signal", bbox_inches='tight', dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()

def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12,6),dpi=300)

    title='Shape of $m(B_{{closer}}^0)$ at different cuts on \n'+r" GBDT1 scores for $\{\mathrm{SB}2^{\mathrm{data}}\}$, $q_{low}^2$"+"\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig,placemenent=121, input_data = NN2trb_lowq2, target = 'B_mass_Kstar_mass_closer', title=title, root_tag='BDT1')

    title='Shape of $m(B_{{closer}}^0)$ at different cuts on \n'+r" GBDT2 scores for $\{\mathrm{SB2}^{\mathrm{data}}\}$, $q_{low}^2$"+"\n Global, Local and Mass Hyp. Selection Rules Applied"

    P.plot_mass(fig=fig,placemenent=122, input_data = NN2trb_lowq2, target = 'B_mass_Kstar_mass_closer', title=title, root_tag='BDT2')
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"Mass_shape_bkg",bbox_inches = 'tight',dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()


def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12, 5), dpi=300)
    title = 'Shape of $m(ll)$ at different cuts on \n' + r" GBDT1 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"

    P.plot_mass(fig=fig, placemenent=121, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='di{}_mass'.format(lepton_big), title=title, root_tag='BDT1', xrange_bins=(1000, 2500, 100), x_label_str="$m(ll)$ $[MeV]$")

    title = 'Shape of $m(ll)$ at different cuts on \n' + r" GBDT2 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=122, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='di{}_mass'.format(lepton_big), title=title, root_tag='BDT2', xrange_bins=(1000, 2500, 100), x_label_str="$m(ll)$ $[MeV]$")
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "Mass_shape_mll", bbox_inches='tight', dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()

# Bd and BdBar Test ----------------------------------------------------------------------------------------------------

print("Starting Bd and BdBar Test")
class Plot_Bd_Bdbar_test():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold=None, root_tag: str = 'BDT1', test_data: bool = False):
        self.input_data = input_data
        self.root_tag = root_tag
        self.test_data = test_data

        if (threshold == None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

    def test(self, fig: mpl.figure.Figure = None, placemenent: int = 131, title: str = None):
        if self.threshold != None:
            PData = self.input_data

            if self.test_data == False:
                PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

            plot_data = PData.eval(f"P_{self.root_tag}_sum")
            P_Bd_BdBar = pd.concat([PData.eval(f"P_{self.root_tag}_Bd"), PData.eval(f"P_{self.root_tag}_BdBar")], axis=1)
            cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

            Bd_truth_mask = (PData.info_is_true_res_Bd == True) | (PData.info_is_true_nonres_Bd == True)
            BdBar_truth_mask = (PData.info_is_true_res_BdBar == True) | (PData.info_is_true_nonres_BdBar == True)

            signal_truth_mask = (PData.info_truth_matching == 1)
            data_truth_mask = (PData.info_truth_matching == 0)

            y_predict = np.array([1 if pred > self.threshold else 0 for pred in plot_data])
            y_predict[(y_predict == 1)] = np.argmax(P_Bd_BdBar.values[(y_predict == 1)], axis=1) + 1
            y_truth = PData.info_truth_matching * ~(Bd_truth_mask | BdBar_truth_mask) + 1 * Bd_truth_mask + 2 * BdBar_truth_mask

            cf_matrix = metrics.confusion_matrix(y_truth, y_predict)
            percen_matrix = metrics.confusion_matrix(y_truth, y_predict, normalize='true')
            group_counts = ["{0:0.0f}".format(value) for value in cf_matrix.flatten()]
            group_percentages = ["{0:.2%}".format(value) for value in percen_matrix.flatten()]

            return percen_matrix[1, 1], percen_matrix[2, 2]

        else:
            print('Threshold/cut is needed for the plotting of a confusion matrix.')

    def plot_mass(self, fig: mpl.figure.Figure = None, placemenent: int = 131, input_data: pd.core.frame.DataFrame = None, target: str = 'B_mass_Kstar_mass_closer', title: str = None, root_tag: str = None, xrange_bins: tuple = (sideband_L, sideband_R, 85), test_data: bool = False, x_label_str: str = "$m(B_{{closer}}^0)$ $[MeV]$"):
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
        PData = self.input_data  # input_data = Data[(Data.info_sample == '300590')]

        if test_data == False:
            # Local Candidates
            PData_LC = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()
        else:
            PData_LC = PData

        def mass_hypotesis_predict(X):
            X['mass_hypotesis'] = 'NaN'
            max_select = np.argmax([X.P_BDT1_sum, X.P_BDT2_sum], axis=0)
            max_select_BDT1 = np.argmax([X.P_BDT1_Bd, X.P_BDT1_BdBar], axis=0)
            max_select_BDT2 = np.argmax([X.P_BDT2_Bd, X.P_BDT2_BdBar], axis=0)
            X['mass_hypotesis'][(max_select == 0) & (max_select_BDT1 == 0)] = 'Bd'
            X['mass_hypotesis'][(max_select == 0) & (max_select_BDT1 == 1)] = 'BdBar'
            X['mass_hypotesis'][(max_select == 1) & (max_select_BDT2 == 0)] = 'Bd'
            X['mass_hypotesis'][(max_select == 1) & (max_select_BDT2 == 1)] = 'BdBar'
            return X

        PData_LC_MH = mass_hypotesis_predict(PData_LC)
        PData_LC_MH_Bd = PData_LC_MH[(PData_LC_MH['mass_hypotesis'] == 'Bd')]
        return PData_LC_MH_Bd

BDT1_bd_res=[]
BDT1_bdbar_res=[]

BDT2_bd_res=[]
BDT2_bdbar_res=[]
range_prob = np.append(np.arange(0,1,0.05),np.arange(0.96,1,0.01))

for i in range_prob:
    A = Plot_Bd_Bdbar_test(input_data=pd.concat([MC_USR_lowq2,NN1trb_lowq2],axis=0),threshold=i,root_tag='BDT1').test()
    BDT1_bd_res.append(A[0])
    BDT1_bdbar_res.append(A[1])
    B = Plot_Bd_Bdbar_test(input_data=pd.concat([MC_USR_lowq2,NN2trb_lowq2],axis=0),threshold=i,root_tag='BDT2').test()
    BDT2_bd_res.append(B[0])
    BDT2_bdbar_res.append(B[1])

A = Plot_Bd_Bdbar_test(input_data=pd.concat([MC_USR_lowq2,NN1trb_lowq2],axis=0),threshold=0.5,root_tag='BDT1').test()
A

fig = plt.figure(figsize=(10, 4), dpi=300)
ax = fig.add_subplot(111)
ax.plot(range_prob, BDT1_bd_res, label='GBDT1 $B_d^0$ efficiency')
ax.plot(range_prob, BDT1_bdbar_res, label='GBDT1 $\overline{B}_d^0$ efficiency')
ax.plot(range_prob, BDT2_bd_res, label='GBDT2 $B_d^0$ efficiency')
ax.plot(range_prob, BDT2_bdbar_res, label='GBDT2 $\overline{B}_d^0$ efficiency')

ax.set_title("GBDT1 efficency in $\{$SR $\cup$ SB1$\}$ and GBDT2 efficency in $\{$SR $\cup$ SB2$\}$, $q_{low}^2$ | Global/Local Selection Rules Applied ")
ax.set_xlabel('Probability', loc='right')
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.legend(loc='lower left', frameon=True)
plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BdBdbar_eff.png", bbox_inches='tight', dpi=300)

plt.show()


def BDT1_response_in_USR__NN1TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN1trb_lowq2], axis=0), threshold=0.4, root_tag='BDT1')

    fig = plt.figure(figsize=(5, 3.5), dpi=300)
    title = ''
    # P.plot_descision_scores(fig=fig,placemenent=221,title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=111, title=title, y_range=(0.98, 1))
    # P.plot_roc_curve(fig=fig,placemenent=223)
    # P.plot_confusionmatrix_3x3(fig=fig,placemenent=224)
    main_title = "GBDT1 Efficiency/Rejection in $\{$SR $\cup$ SB1$\}$, $q_{low}^2$,\nSignal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_USR_NN1TRB_lowq2_eff_rej", bbox_inches='tight', dpi=300)

    plt.show()


BDT1_response_in_USR__NN1TRB()


def BDT2_response_in_USR__NN2TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN2trb_lowq2], axis=0), threshold=0.4, root_tag='BDT2')

    fig = plt.figure(figsize=(5, 3.5), dpi=300)
    title = ''
    # P.plot_descision_scores(fig=fig,placemenent=221,title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=111, title=title, y_range=(0.98, 1))
    # P.plot_roc_curve(fig=fig,placemenent=223)
    # P.plot_confusionmatrix_3x3(fig=fig,placemenent=224)
    main_title = "GBDT2 Efficiency/Rejection in $\{$SR $\cup$ SB2$\}$, $q_{low}^2$,\nSignal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_USR_NN2TRB_lowq2_eff_rej", bbox_inches='tight', dpi=300)

    plt.show()


BDT2_response_in_USR__NN2TRB()