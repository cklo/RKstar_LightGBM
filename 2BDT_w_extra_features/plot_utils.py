import pandas as pd
import numpy as np
import lightgbm
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.special import logit, expit
from sklearn import metrics
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import sklearn.inspection
from sklearn import preprocessing, model_selection, metrics
from sklearn.preprocessing import LabelBinarizer
from sklearn.base import BaseEstimator
import toml

from typing import Optional

# ________________________________________________________________________
""" 
Plot control from training phase of verstack output library.
"""
# ________________________________________________________________________
# Loading config -------------------------------------------------------------------------------------------------------
config = toml.load('config.toml')
channel = config['channel']['channel']
#-----------------------------------------------------------------------------------------------------------------------

if channel == "electron":
    treeName = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix = "BeeKst"
    lepton = "electron"
    lepton_big = "Electron"
    lepton_short = "e"
    antilepton = "positron"
    sideband_L = 4000.
    sideband_R = 5700.
    meson_ptcut = 500
    lepton_ptcut = 5000
    lepton_etacut = 2.5
    meson_etacut = 2.5
    Kst_cut_L = 690
    Kst_cut_H = 1110
    LowBmass_cut = 3000
    HighBmass_cut = 6500

elif channel == "muon":
    treeName = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix = "BmumuKst"
    lepton = "muon"
    lepton_big = "Muon"
    lepton_short = "mu"
    antilepton = "antimuon"
    sideband_L = 5000.
    sideband_R = 5700.
    meson_ptcut = 500
    lepton_ptcut = 6000  # !
    lepton_etacut = 2.5
    meson_etacut = 2.5
    Kst_cut_L = 690
    Kst_cut_H = 1110
    LowBmass_cut = 4700  # !
    HighBmass_cut = 6000  # !
else:
    print("Channel should be either electron or muon")
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"


class Verstack_train_plots():
    def __init__(self):
        pass

    def plot_optimization_history(self,df_trials:pd.core.frame.DataFrame,fig=None,placement=131):
        ax_hist = fig.add_subplot(placement)

        df_plot = df_trials[df_trials.state == 'COMPLETE']
        direction = 'MINIMIZE' # or 'MAXIMIZE'
        if direction == 'MINIMIZE':
            best_values = np.minimum.accumulate(df_plot.value.values)
        else:
            best_values = np.maximum.accumulate(df_plot.value.values)

        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']
        
        ax_hist.scatter(x=df_plot.number.values,y=df_plot.value.values,color=clist[1],alpha=1,label="Objective Value",)
        ax_hist.plot(df_plot.number.values, best_values, marker="o",color=clist[0],alpha=0.5,label="Best Value",)
        ax_hist.set(title="Optimization History Plot",xlabel="#Trials",ylabel="Objective Value")
        ax_hist.legend()

    def plot_param_imp(self,df_importance:pd.core.frame.DataFrame,fig=None,placement = 132):
        ax_param = fig.add_subplot(placement)

        Importances = df_importance.sort_values('value')
        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']

        ax_param.barh(list(Importances.index), Importances.value.values,fill=False, align="center",edgecolor=clist[0],linewidth=2,tick_label=list(Importances.index))
        for tick in ax_param.yaxis.get_majorticklabels():
            tick.set_horizontalalignment("left")
        ax_param.tick_params(axis="y",direction="out", pad=-10,labelcolor='black',size=5)
        ax_param.set(title="Hyperparameter Importances",ylabel="Hyperparameter",xlabel="Importance for Objective Value")

    def plot_intermediate(self, df_trials:pd.core.frame.DataFrame,fig=None, placement=133):
        ax_inter = fig.add_subplot(placement)

        df_plot = df_trials.sort_values('value')

        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']

        for i in range(1,len(df_plot)-1):
            ax_inter.plot(list(df_plot.iloc[i].intermediate_values.index) ,df_plot.iloc[1].intermediate_values.values,c='black',alpha=0.7)
        ax_inter.plot(list(df_plot.iloc[0].intermediate_values.index),df_plot.iloc[0].intermediate_values.values,c=clist[0],label='Best Trial')
        
        ax_inter.set(title="Intermediate Values Plot",ylabel="Intermediate Value",xlabel="Step")
        ax_inter.legend()


# ________________________________________________________________________
""" 
Response Plots
"""
# ________________________________________________________________________



class plot_response():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold: Optional[float]=None, root_tag:str='BDT1',test_data:bool=False):
        self.input_data = input_data
        self.root_tag = root_tag
        self.test_data = test_data

        if (threshold== None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

        needed_columns = [f'P_{root_tag}_Bkg', f'P_{root_tag}_Bd', f'P_{root_tag}_BdBar', f'P_{root_tag}_sum',f'P_{root_tag}_max', 'info_truth_matching', 'info_is_true_nonres_Bd', 'info_is_true_res_Bd', 'info_is_true_res_BdBar', 'info_is_true_nonres_BdBar','info_event_number']
        assert set(needed_columns) == set([i for i in needed_columns if i in list(input_data.columns)]), f"The following columns are not in the input data:\n{[i for i in needed_columns if i not in list(input_data.columns)]}"
        
    def plot_descision_scores(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        PData = self.input_data

        if self.test_data == False:
            PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

        plot_data = PData.eval(f"P_{self.root_tag}_sum")

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)

        x_range = (logit(PData.eval(f"P_{self.root_tag}_sum.min()")), logit(PData.eval(f"P_{self.root_tag}_sum.max()")))
        bins = 100
        bindwidth = (x_range[1]-x_range[0])/bins
        bindwidth_str = str(float(f"{bindwidth:.1g}")).split('.')[0] if float(str(float(f"{bindwidth:.1g}")).split('.')[-1]) == 0 else str(float(f"{bindwidth:.1g}"))

        ax_scores = fig.add_subplot(placemenent)
        c1,_,_ =ax_scores.hist(logit(plot_data[data_truth_mask]), bins=bins, range=x_range,alpha=0.65,edgecolor="k",label='Background',density=True,log=True)
        c2,c__2,_ =ax_scores.hist(logit(plot_data[signal_truth_mask]), bins=bins, range=x_range, alpha=0.65,edgecolor="k",label=f"Signal",density=True,log=True)
        if self.threshold != None:
            ax_scores.vlines(logit(self.threshold),0,max(c1+c2),color='black',linestyle=':',label=f"Decision Score Cut: {self.threshold:.3f} | Logit: {logit(self.threshold):.3f}")

        if title != None:
            ax_scores.set_title(title)
        ax_scores.set_ylim(0,max(c1+c2)*100)
        ax_scores.set_xlabel('Probability (Logit Transformed)',loc='right')
        ax_scores.set_ylabel(f"Density / ({bindwidth_str})",loc='top')
        ax_scores.legend(loc = 'upper right', frameon=True)

    def Plot_efficiency_rejection(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None,y_range:tuple=(0,1)):
        PData = self.input_data

        if self.test_data == False:
            PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

        plot_data = PData.eval(f"P_{self.root_tag}_sum")
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)

        hist_eff = []
        hist_rej = []

        lin_space_range = np.linspace(0,1,1001)

        for cut in lin_space_range:
            hist_eff.append(len(plot_data[signal_truth_mask][(plot_data[signal_truth_mask] > cut)]) / len(plot_data[signal_truth_mask]))
            hist_rej.append(1-len(plot_data[data_truth_mask][(plot_data[data_truth_mask] > cut )]) / len(plot_data[data_truth_mask]))

        ax_eff = fig.add_subplot(placemenent)
        plot_eff = ax_eff.plot(lin_space_range,hist_eff,label='Signal (Effeciency)',color=cycle[0])

        ax_eff_y= ax_eff.twinx()

        ax_eff.set_xlabel('Probability',loc='right')
        ax_eff.set_ylabel('Efficiency',loc='top',color=cycle[0])
        ax_eff_y.set_ylabel('Rejection',loc='top',color=cycle[1])
        
        ax_eff_y.set_ylim(y_range)
        ax_eff_y.set_yticks(np.linspace(y_range[0],y_range[1],5))
        ax_eff_y.tick_params(axis='y', labelsize = 8)


        ax_eff.set_ylim(0,1)
        ax_eff.set_yticks(np.linspace(0,1,5))

        plot_rej = ax_eff_y.plot(lin_space_range,hist_rej,label='Background (rejection)',color=cycle[1])

        if title != None:
            ax_eff.set_title(title)
        
        leg = plot_eff + plot_rej
        labs = [l.get_label() for l in leg]


        ax_eff.legend(leg,labs,frameon=True,loc='lower center')

    def plot_roc_curve(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):

        PData = self.input_data
        
        if self.test_data == False:
            PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

        plot_data = PData.eval(f"P_{self.root_tag}_sum")
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)
        y_truth = PData.info_truth_matching

        ax_roc = fig.add_subplot(placemenent)

        fpr, tpr, _ = metrics.roc_curve(y_truth, plot_data)                  # False/True Positive Rate for our model
        auc_score = metrics.auc(fpr,tpr) 
        if self.threshold != None:
            tpr_thres=(sum(map(lambda x : x>self.threshold, plot_data[signal_truth_mask])))/len(plot_data[signal_truth_mask])
            fpr_thres=(sum(map(lambda x : x>self.threshold, plot_data[data_truth_mask])))/len(plot_data[data_truth_mask])

        ax_roc.plot(fpr, tpr, label=f'Area Under Curve (AUC) score: {auc_score:5.3f}',color=cycle[0])
        if self.threshold != None:
            ax_roc.scatter(fpr_thres,tpr_thres,s=40, color=cycle[0],label=f"Threshold Cut |"+r" (FPR,TPR)$=({fpr:.2f},{tpr:.2f})$".format(fpr=fpr_thres,tpr=tpr_thres))

        prec, recall, _ = metrics.precision_recall_curve(y_truth, plot_data) 
        if self.threshold != None:
            precision_thres = metrics.precision_score(y_truth, plot_data > self.threshold)
            recall__thres = metrics.recall_score(y_truth, plot_data > self.threshold)
        AP=metrics.average_precision_score(y_truth, plot_data)

        ax_roc.plot(recall, prec, label=f'Average Precision (AP) score: {AP:5.3f}',color=cycle[1])
        if self.threshold != None:
            ax_roc.scatter(recall__thres,precision_thres,s=40, color=cycle[1],label=f"Threshold Cut |"+r" (TPR,PPV)$=({trp:.2f},{ppv:.2f})$".format(trp=recall__thres,ppv=precision_thres))

        if title != None:
            ax_roc.set_title(title)                  
        ax_roc.set_xlabel('False Postive Rate: '+r'$\frac{\mathrm{False Positive}}{\mathrm{Actual Negative}}$',loc='right',color=cycle[0])
        ax_roc.set_ylabel('True Positive Rate: '+r'$\frac{\mathrm{True Positive}}{\mathrm{True Positive}+}$',loc='top',color=cycle[0])

        ax_roc_x, ax_roc_y=ax_roc.twiny(), ax_roc.twinx()
        ax_roc_x.set_xticks([]), ax_roc_y.set_yticks([])
        ax_roc_x.grid(False),  ax_roc_y.grid(False)
        ax_roc_x.set_xlabel('Recall: '+r'$\frac{\mathrm{True Positive}}{\mathrm{Actual Positive}}$',loc='right',color=cycle[1], labelpad=5.5)
        ax_roc_y.set_ylabel('Precision: '+r'$\frac{\mathrm{True Positive}}{\mathrm{Predicted Positive}}$',loc='top',color=cycle[1])

        ax_roc.legend(frameon=True,loc='lower center')

    def plot_confusionmatrix_3x3(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        if self.threshold != None:
            PData = self.input_data

            if self.test_data == False:
                PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

            plot_data = PData.eval(f"P_{self.root_tag}_sum")
            P_Bd_BdBar=pd.concat([PData.eval(f"P_{self.root_tag}_Bd"),PData.eval(f"P_{self.root_tag}_BdBar")],axis=1)
            cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

            Bd_truth_mask = (PData.info_is_true_res_Bd == True) | (PData.info_is_true_nonres_Bd == True)
            BdBar_truth_mask = (PData.info_is_true_res_BdBar == True) | (PData.info_is_true_nonres_BdBar == True)

            signal_truth_mask = (PData.info_truth_matching == 1)
            data_truth_mask = (PData.info_truth_matching == 0)

            y_predict  = np.array([1 if pred > self.threshold else 0 for pred in plot_data])
            y_predict[(y_predict==1)]=np.argmax(P_Bd_BdBar.values[(y_predict==1)],axis=1)+1
            y_truth = PData.info_truth_matching * ~(Bd_truth_mask | BdBar_truth_mask) + 1 * Bd_truth_mask + 2 * BdBar_truth_mask
                
            ax_conf = fig.add_subplot(placemenent)
            cf_matrix = metrics.confusion_matrix(y_truth, y_predict) 
            percen_matrix = metrics.confusion_matrix(y_truth, y_predict, normalize='true') 
            group_counts = ["{0:0.0f}".format(value) for value in cf_matrix.flatten()]
            group_percentages = ["{0:.2%}".format(value) for value in percen_matrix.flatten()]

            #labels = np.asarray([f"{v1}\n{v2}\n{v3}" for v1, v2, v3 in zip(group_names,group_counts,group_percentages)]).reshape(2,2)
            labels = np.asarray([f"{v1}\n{v2}" for v1, v2 in zip(group_counts,group_percentages)]).reshape(3,3)
            ax_conf = sns.heatmap(cf_matrix, annot=labels, fmt='', cmap='Blues',cbar=False)
            ax_conf.set(xlabel='Predicted Values',ylabel='True Values')
            ax_conf.xaxis.set_ticklabels(['Background',r'$B_d^0$',r'$\overline{B}_d^0$'])
            ax_conf.yaxis.set_ticklabels(['Background',r'$B_d^0$',r'$\overline{B}_d^0$'])
            if title != None:
                ax_conf.set_title(title)
        else:
            print('Threshold/cut is needed for the plotting of a confusion matrix.')

# ________________________________________________________________________
""" 
Response Plots (2D)
"""
# ________________________________________________________________________

class TwoD_response_curves():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold: Optional[float]=None, root_tag:str='BDT1', test_data=False):
        self.input_data = input_data
        self.root_tag = root_tag
        self.test_data = test_data

        if (threshold== None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/Cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

        needed_columns = [f'P_{root_tag}_Bkg', f'P_{root_tag}_Bd', f'P_{root_tag}_BdBar', f'P_{root_tag}_sum',f'P_{root_tag}_max', 'info_truth_matching', 'info_is_true_nonres_Bd', 'info_is_true_res_Bd', 'info_is_true_res_BdBar', 'info_is_true_nonres_BdBar']
        assert set(needed_columns) == set([i for i in needed_columns if i in list(input_data.columns)]), f"The following columns are not in the input data:\n{[i for i in needed_columns if i not in list(input_data.columns)]}"
        
    def plot_Bd(self,fig:mpl.figure.Figure=None,placemenent:int=121, title:str=None):
        PData = self.input_data
        
        if self.test_data == False:
                PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

        Bd_truth_mask = (PData.info_is_true_res_Bd == True) | (PData.info_is_true_nonres_Bd == True)

        ax_Bd = fig.add_subplot(placemenent)
        h = ax_Bd.hist2d(PData.eval(f"P_{self.root_tag}_Bd")[Bd_truth_mask],PData.eval(f"P_{self.root_tag}_BdBar")[Bd_truth_mask],bins = list(np.linspace(0,1,25)),
                density = True,
                norm = mpl.colors.LogNorm(),
                cmap = plt.cm.Reds)
        fig.colorbar( h[3], ax = ax_Bd)
        ax_Bd.set_xlabel( r"Probability to be $B_d^0$", loc = "right")
        ax_Bd.set_ylabel( r"Probability to be $\overline{B}_d^0$", loc = "top")
        if title != None:
            ax_Bd.set_title(title)
        else:
            ax_Bd.set_title(r'MC sample: $B_d^0$')

    def plot_BdBar(self,fig:mpl.figure.Figure=None,placemenent:int=122, title:str=None):
        PData = self.input_data

        if self.test_data == False:
                PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()
        
        BdBar_truth_mask = (PData.info_is_true_res_BdBar == True) | (PData.info_is_true_nonres_BdBar == True)

        ax_BdBar = fig.add_subplot(placemenent)
        h = ax_BdBar.hist2d(PData.eval(f"P_{self.root_tag}_Bd")[BdBar_truth_mask],PData.eval(f"P_{self.root_tag}_BdBar")[BdBar_truth_mask],bins = list(np.linspace(0,1,25)),
                density = True,
                norm = mpl.colors.LogNorm(),
                cmap = plt.cm.Reds)
        fig.colorbar( h[3], ax = ax_BdBar)
        ax_BdBar.set_xlabel( r"Probability to be $B_d^0$", loc = "right")
        ax_BdBar.set_ylabel( r"Probability to be $\overline{B}_d^0$", loc = "top")
        if title != None:
            ax_BdBar.set_title(title)
        else:
            ax_BdBar.set_title(r'MC sample: $\overline{B}_d^0$')


# ________________________________________________________________________
""" 
Compare Res vs NonRes in efficinecy
"""
# ________________________________________________________________________

class plot_res_vs_nonres():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold: Optional[float]=None,test_data:bool=False):
        self.input_data = input_data
        self.test_data = test_data

        if (threshold== None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

        #needed_columns = [f'P_{root_tag}_Bkg', f'P_{root_tag}_Bd', f'P_{root_tag}_BdBar', f'P_{root_tag}_sum',f'P_{root_tag}_max', 'info_truth_matching', 'BeeKst_isTrueNonresBd', 'BeeKst_isTrueResBd', 'BeeKst_isTrueResBdbar', 'BeeKst_isTrueNonresBdbar','info_event_number','info_sample']
        #assert set(needed_columns) == set([i for i in needed_columns if i in list(input_data.columns)]), f"The following columns are not in the input data:\n{[i for i in needed_columns if i not in list(input_data.columns)]}"

    def Plot_2efficiency(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None,root_tag:str=None):
        PData = self.input_data

        if self.test_data == False:
            PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

        plot_data = PData.eval(f"P_{root_tag}_sum")
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)
        NonRes_truth_mask = ((PData.info_is_true_res_Bd == True) | (PData.info_is_true_nonres_Bd == True)) & ((PData.info_sample == mc_NonresBd_str) | (PData.info_sample == mc_NonresBdbar_str))
        Res_truth_mask = ((PData.info_is_true_res_BdBar == True) | (PData.info_is_true_nonres_BdBar == True)) & ((PData.info_sample == mc_ResBd_str) | (PData.info_sample == mc_ResBdbar_str))

        hist_eff_NonRes = []
        hist_rej_NonRes = []

        hist_eff_Res = []
        hist_rej_Res = []

        lin_space_range = np.linspace(0,1,1001)

        for cut in lin_space_range:
            hist_eff_NonRes.append(len(plot_data[NonRes_truth_mask][(plot_data[NonRes_truth_mask] > cut)]) / len(plot_data[NonRes_truth_mask]))
            hist_eff_Res.append(len(plot_data[Res_truth_mask][(plot_data[Res_truth_mask] > cut)]) / len(plot_data[Res_truth_mask]))

            #hist_rej.append(1-len(plot_data[data_truth_mask][(plot_data[data_truth_mask] > cut )]) / len(plot_data[data_truth_mask]))

        ax_eff = fig.add_subplot(placemenent)
        #ax_eff.plot(lin_space_range,hist_rej,label='Read Data (rejection)',color=cycle[1])
        ax_eff.plot(lin_space_range,hist_eff_NonRes,label='Non-res Signal (effeciency)',color=cycle[0])
        ax_eff.plot(lin_space_range,hist_eff_Res,label='Res Signal (effeciency)',color=cycle[1])


        ax_eff_y= ax_eff.twinx()
        ax_eff_y.set_yticks([]), ax_eff_y.grid(False)

        ax_eff.set_xlabel('Probability',loc='right')
        ax_eff.set_ylabel('Efficiency',loc='top')
        #ax_eff_y.set_ylabel('Rejection',loc='top',color=cycle[1])
        if title != None:
            ax_eff.set_title(title)
        ax_eff.legend(frameon=True,loc='lower center')

# ________________________________________________________________________
""" 
MASS Shapes
"""
# ________________________________________________________________________
class plot_mass_shape_in_USR():
    def __init__(self,test_data:bool=False):
        self.test_data = test_data

    def plot_mass(self,fig:mpl.figure.Figure=None,placemenent:int=131, input_data: pd.core.frame.DataFrame = None, target:str = 'B_mass_Kstar_mass_closer', title:str=None,root_tag:str=None,xrange_bins:tuple=(4000,5700,85),test_data:bool=False,x_label_str:str ="$m(B_{{closer}}^0)$ $[MeV]$" ):
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
        PData = input_data # input_data = Data[(Data.info_sample == '300590')]

        if test_data == False:
            #Local Candidates
            PData_LC = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()
        else:
            PData_LC = PData

        def mass_hypotesis_predict(X):
            X['mass_hypotesis'] = 'NaN'
            max_select = np.argmax([X.P_BDT1_sum, X.P_BDT2_sum],axis=0)
            max_select_BDT1 = np.argmax([X.P_BDT1_Bd, X.P_BDT1_BdBar],axis=0)
            max_select_BDT2 = np.argmax([X.P_BDT2_Bd, X.P_BDT2_BdBar],axis=0)
            X['mass_hypotesis'][(max_select==0)&(max_select_BDT1==0)] = 'Bd'
            X['mass_hypotesis'][(max_select==0)&(max_select_BDT1==1)] = 'BdBar'
            X['mass_hypotesis'][(max_select==1)&(max_select_BDT2==0)] = 'Bd'
            X['mass_hypotesis'][(max_select==1)&(max_select_BDT2==1)] = 'BdBar'
            return X
            
        PData_LC_MH=mass_hypotesis_predict(PData_LC)
        PData_LC_MH_Bd=PData_LC_MH[(PData_LC_MH['mass_hypotesis']=='Bd')]


        ax_main = fig.add_subplot(placemenent)
        ax_main.axis('off')  # command for hiding the axis.
        Sub_top = inset_axes(ax_main, width="100%", height="67.5%", loc=9)
        Sub_bottom = inset_axes(ax_main, width="100%", height="27.5%", loc=8)
        Sub_top.set_xticks([])
        Sub_top.sharex(Sub_bottom)
        Sub_top.tick_params(labelbottom=False)
        Sub_top.spines['bottom'].set_visible(True)

        xrange=xrange_bins[:2]
        bins= xrange_bins[2]
        bindwidth = (xrange[1]-xrange[0])/bins
        bindwidth_str = str(float(f"{bindwidth:.1g}")).split('.')[0] if float(str(float(f"{bindwidth:.1g}")).split('.')[-1]) == 0 else str(float(f"{bindwidth:.1g}"))

        #bench, edges,_ = Sub_top.hist(PData_LC_MH_Bd.eval(f"{target}"),bins=bins,range=xrange, density=True,histtype = 'step', fill = None, label='Benchmark (NonRes Bd)',lw=1.5,color=cycle[0])
        bench, edges = np.histogram(PData_LC_MH_Bd.eval(f"{target}"),bins=bins,range=xrange,density=False)

        centers  = 0.5*(edges[1:] + edges[:-1])
        heights = []
        heights_max = []
        cut_range = [0,0.05,0.1,0.3,0.5,0.7,0.9]
        for i in range(len(cut_range)):
            cut = cut_range[i]
            bin_hights_max ,_,_ = Sub_top.hist(PData_LC_MH_Bd.eval(f"{target}")[(PData_LC_MH_Bd.eval(f"P_{root_tag}_sum") > cut)],bins=bins,range=xrange,density=True,histtype = 'step', fill = None, label=f"$[P_{{{root_tag}}}(B_d^0)+P_{{{root_tag}}}(\overline{{B}}_d^0)] > {cut}$",lw=1.5,color=cycle[i+1])
            bin_hights ,_ = np.histogram(PData_LC_MH_Bd.eval(f"{target}")[(PData_LC_MH_Bd.eval(f"P_{root_tag}_sum") > cut)],bins=bins,range=xrange,density=False)

            heights_max.append(bin_hights_max)          
            heights.append(bin_hights)
        
        effs_iter_max = []
        effs_iter_min = []
        for i in range(1,len(heights)):
            bench = heights[0]
            Eff=np.divide(heights[i],bench)
            Eff_xerr=(edges[1] - edges[0] )/2
            Eff_yerr=np.sqrt(np.divide(np.multiply(Eff, np.subtract(np.ones(Eff.size), Eff)), bench)) #Binomial error
        
            Eff_yerr, centers_Eff, Eff=Eff_yerr[~np.isnan(Eff)], centers[~np.isnan(Eff)], Eff[~np.isnan(Eff)]
        
            mask_mean = (Eff_yerr)!=0
            yrr,Xrr = Eff_yerr[mask_mean],Eff[mask_mean]
            weight,weight_mask = np.divide(1,yrr**2), ~np.isnan(np.divide(1,yrr**2))

            #mean = np.average(Xrr[weight_mask],weights=weight[weight_mask])
            #mean_std = np.sqrt(np.average((Xrr[weight_mask]-mean)**2, weights=weight[weight_mask]))
            mean = np.mean(Eff)
            mean_std = np.std(Eff)


            label_eff = f'Mean = {mean:.3f}, std = {mean_std:.3f}'
            Sub_bottom.errorbar(centers_Eff, Eff,yerr=Eff_yerr, xerr=Eff_xerr,fmt="none",label=label_eff,color=cycle[i+1])

            effs_iter_max.append(np.max(Eff))
            effs_iter_min.append(np.min(Eff))

        Sub_top.set_title(title)
        Sub_top.set_ylim(0,np.max(heights_max)*1.35)

        Sub_top.legend(loc='upper left',frameon=True)
        Sub_bottom.set_xlabel(x_label_str,loc='right')

        Sub_top.set_ylabel(f"Density / ({bindwidth_str})",loc='top')
        Sub_bottom.set_ylabel(f"Efficiency",loc='top')
        
        Sub_bottom.set_ylim(np.max([0.0,np.min(effs_iter_min)*0.5]),np.min([1.1,np.max(effs_iter_max)*1.5]))
        Sub_bottom.legend(loc = 'lower left', frameon=True,fontsize=8,ncol=2)
        Sub_top.ticklabel_format(axis="y", style="sci", scilimits=(0,0))