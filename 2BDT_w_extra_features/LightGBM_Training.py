# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import plot_utils as plotting
import toml
from sklearn import preprocessing, model_selection, metrics, decomposition
import seaborn as sns
import random

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit
import os

from sklearn.preprocessing import MinMaxScaler, StandardScaler, LabelBinarizer
#import verstack
import Verstack_cluster_safe.LGBMTuner as verstack
import optuna
import sklearn.inspection
from sklearn.base import BaseEstimator
random.seed(42)

mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

# Loading config -------------------------------------------------------------------------------------------------------
config = toml.load('config.toml')
channel = config['channel']['channel']
#-----------------------------------------------------------------------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

# Loading features -----------------------------------------------------------------------------------------------------
Storage = Utils.HDF5_IO(config['PATHS_{}'.format(channel)]['Storage'])
#safe_good_features = ['tracks_dEdx_diff', 'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
#safe_good_features = ['Lxy_significance_over_B_chi2','tracks_dEdx_diff','diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
safe_good_features = ['tracks_dEdx_diff', 'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]

Features_to_be_read = []
for k, v in config['features_{}'.format(channel)].items():
    Features_to_be_read.extend(v)
Features_to_be_read = Features_to_be_read + safe_good_features
features = config['features_{}'.format(channel)]['ML_features']
features = features + safe_good_features

# Loading Data ---------------------------------------------------------------------------------------------------------

def load_multiple_feather_files(file_list:list, selection:str, N_events:int, columns_to_read:list):
    print('Starts loading files..')
    files = []
    List_uniques_list = []
    for file_path in file_list:
        files.append(pd.read_feather(path=file_path, columns=columns_to_read, use_threads=True, storage_options=None))
    print('Done loading..')

    if selection == "None":
        for i in range(len(files)):
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]
    else:
        for i in range(len(files)):
            files[i] = files[i].query(selection)
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]

    print(f"Number of uniques: {[len(i) for i in List_uniques_list]}, total = {sum([len(i) for i in List_uniques_list])}")
    print(f"Length of files: {[len(i) for i in files]}, total = {sum([len(i) for i in files])}")

    df_files = pd.concat(files)
    print('Done..')
    print()

    return df_files


if channel == "electron":
    files_dictionary = {
        'MC_signal': [os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_03.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300590_part_04.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300591_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300591_part_02.ftr')],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if config['DATA_{}'.format(channel)]['selection'] in x]) # Using 10 ntuples (PeriodK) for testing
        }
elif channel == "muon":
    files_dictionary = {
        'MC_signal': [os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300700_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300700_part_02.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300701_part_01.ftr'),
                      os.path.join(config['PATHS_{}'.format(channel)]['feather'], 'ntuple-300701_part_02.ftr')],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if config['DATA_{}'.format(channel)]['selection'] in x]) # Using 10 ntuples (PeriodK) for testing
    }

MC_USR_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = Utils.selection_cuts()['usr_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(MC_USR_lowq2)

NN1trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'],
    selection = Utils.selection_cuts()['nn1trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(NN1trb_lowq2)
# Select 6 best Candidates among events
NN1trb_lowq2 = NN1trb_lowq2.assign(Lxy_significance_over_B_chi2=lambda x: x['Lxy_significance'] / x['B_chi2']).sort_values("Lxy_significance_over_B_chi2").groupby("info_event_number").tail(6).sort_index()
print(NN1trb_lowq2)

NN2trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'],
    selection = Utils.selection_cuts()['nn2trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(NN2trb_lowq2)
# Select 6 best Candidates among events
NN2trb_lowq2 = NN2trb_lowq2.assign(Lxy_significance_over_B_chi2=lambda x: x['Lxy_significance'] / x['B_chi2']).sort_values("Lxy_significance_over_B_chi2").groupby("info_event_number").tail(6).sort_index()
print(NN2trb_lowq2)

def Data_loader():
    USR = MC_USR_lowq2
    USR.drop(USR[~((USR.info_is_true_nonres_Bd == True) | (USR.info_is_true_nonres_BdBar == True))].index, inplace=True)
    USR.drop(USR[(USR.info_is_true_nonres_Bd == True)].index[config['ML_config']['N_Signal']['Bd']:], inplace=True)
    USR.drop(USR[(USR.info_is_true_nonres_BdBar == True)].index[config['ML_config']['N_Signal']['BdBar']:], inplace=True)

    label = (USR.info_sample == 0) * 0
    for i in config['labels_scheme_{}'.format(channel)]:
        label += (USR.info_sample == str(i)) * config['labels_scheme_{}'.format(channel)][str(i)]
    USR['label'] = label

    SB = NN1trb_lowq2
    SB.drop(SB.index[config['ML_config']['N_background']:], inplace=True)
    label = (SB.info_sample == 0) * 0
    for i in config['labels_scheme_{}'.format(channel)]:
        label += (SB.info_sample == str(i)) * config['labels_scheme_{}'.format(channel)][str(i)]
    SB['label'] = label

    SR = NN2trb_lowq2
    SR.drop(SR.index[config['ML_config']['N_background']:], inplace=True)
    label = (SR.info_sample == 0) * 0
    for i in config['labels_scheme_{}'.format(channel)]:
        label += (SR.info_sample == str(i)) * config['labels_scheme_{}'.format(channel)][str(i)]
    SR['label'] = label

    print("Data_loader Finished")
    return USR, SB, SR

USR, SB, SR = Data_loader()

# One-Component PCA (feature extraction, other possible directions: kernal PCA, tSNE, LDA, autoencoder ...) ------------

def FIT_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton),'{}_eta'.format(lepton),'{}_phi'.format(lepton),'{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton),'{}_eta'.format(antilepton),'{}_phi'.format(antilepton),'{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT','trackPlus_eta','trackPlus_phi','trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT','trackMinus_eta','trackMinus_phi','trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance",  "z0_significance",  "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double),"d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson= ["diMeson_angle_alpha_sym",  "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    Utils.fit_PCA_to_train(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_antilepton],n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackPlus],n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackMinus],n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dimeson],n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_quality],n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    Utils.fit_PCA_to_train(Storage,_Data[branch_track1_track2],n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton_dimeson],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dilepton],n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dimeson],n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

def TRANSFORM_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton), '{}_eta'.format(lepton), '{}_phi'.format(lepton), '{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton), '{}_eta'.format(antilepton), '{}_phi'.format(antilepton), '{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT', 'trackPlus_eta', 'trackPlus_phi', 'trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT', 'trackMinus_eta', 'trackMinus_phi', 'trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance", "z0_significance", "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double), "d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson = ["diMeson_angle_alpha_sym", "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    _Data['branch_{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    _Data['branch_{}'.format(antilepton)] = Utils.PCA_transform(Storage,_Data[branch_antilepton], n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    _Data['branch_trackPlus'] = Utils.PCA_transform(Storage,_Data[branch_trackPlus], n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    _Data['branch_trackMinus'] = Utils.PCA_transform(Storage,_Data[branch_trackMinus], n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    _Data['branch_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    _Data['branch_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_dimeson], n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    _Data['branch_b_meson_quality'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_quality], n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    _Data['branch_track1_track2'] = Utils.PCA_transform(Storage,_Data[branch_track1_track2], n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    _Data['branch_di{}_dimeson'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton_dimeson], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    _Data['branch_b_meson_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dilepton], n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    _Data['branch_b_meson_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dimeson], n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

    return _Data

# plotting input features ----------------------------------------------------------------------------------------------

def plot_pre_processed_data(data: list = [], labels: list = [], n_cols: int = 5):
    assert len(data) > 0, 'Data list is empty.'
    assert len(labels) == len(data), 'label list and data list is not equal.'
    assert np.all([item.shape[1] for item in data]), 'All elements in data list has to have same amount of columns.'
    for i in range(len(data)):
        assert list(data[0].columns) == list(data[i].columns), 'Not all columns are equal'

    fig = plt.figure(figsize=(20, 25), dpi=300, tight_layout=True)
    # fig = plt.figure(figsize=(12,5),dpi=300, tight_layout=True)

    fig.suptitle('Input features (normalized)', fontsize=25)

    feature_names = list(data[0].columns)

    nrows = data[0].shape[1] // n_cols + (data[0].shape[1] % n_cols > 0)

    for i in range(data[0].shape[1]):

        # add a new subplot iteratively using nrows and cols
        ax = plt.subplot(nrows, n_cols, i + 1)

        # filter df and plot ticker on the new subplot axis

        plot_bottom = np.array([np.percentile(item.iloc[:, i], 1) for item in data])
        plot_top = np.array([np.percentile(item.iloc[:, i], 99) for item in data])
        ypad = 0.03 * (plot_top - plot_bottom)

        min_range = np.min(plot_bottom) - np.max(ypad)
        max_range = np.max(plot_top) + np.min(ypad)
        Bin = 100
        bindwidth = (max_range - min_range) / Bin

        bindwidth_str = str(float(f"{bindwidth:.1g}")).split('.')[0] if float(str(float(f"{bindwidth:.1g}")).split('.')[-1]) == 0 else str(float(f"{bindwidth:.1g}"))

        plot = [item.iloc[:, i] for item in data]
        for j in range(len(plot)):
            ax.hist(plot[j], bins=Bin, range=(min_range, max_range), alpha=0.5, label=labels[j], density=True)

        ax.set_ylabel(f"Density / ({bindwidth_str})", loc='top', size=12)

        ax.set(yscale='log')
        # ax.set(yticks=[],xticks=[])
        ax.set_xlabel(feature_names[i], fontsize=15, labelpad=0.8)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax.tick_params(axis='both', which='minor', labelsize=12)
        # ax.yaxis.set_major_locator(mpl.ticker.LogLocator(numticks=5))

    plt.tight_layout(pad=0.2)
    fig.subplots_adjust(top=0.96, bottom=0.02)

    # ax.legend(bbox_to_anchor=(0.45,-0.05),    #bbox_transform=fig.transFigure, mode="expand", ncol=1, fontsize = 20)
    ax.legend(bbox_to_anchor=(0.5, 0.0), bbox_transform=fig.transFigure, mode="expand", ncol=1, fontsize=20)

    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "X_train_features.pdf", bbox_inches='tight', dpi=300)

    plt.show()

plot_pre_processed_data(data=[USR[features], SB[features], SR[features]], labels=['Signal Region (SR)', 'Sideband1 (SB1)', 'Sideband1 (SB2)'])

cpu_count = int(np.where(os.cpu_count() < 0, 1, os.cpu_count()))
print('CPU count: '+ str(cpu_count))

# Training GBDT1 -------------------------------------------------------------------------------------------------------

BDT1 = pd.concat([USR,SB])

def train_test_split_stratified(X):
    ratios = {
        'train': 0.7,
        'valid': 0.0,
        'test': 0.30}
    np.random.seed(42)
    X_train, X_rem, y_train, y_rem = model_selection.train_test_split(X, X.label, train_size=ratios['train'],stratify=X.label, shuffle=True,random_state=42)
    if ratios['valid'] == 0:
        X_test = X_rem
        y_test = y_rem
        return {'X_train': X_train, 'y_train': y_train, 'X_test': X_test, 'y_test': y_test}
    else:
        X_valid, X_test, y_valid, y_test = model_selection.train_test_split(X_rem, y_rem, test_size=(ratios['test']/(1-ratios['train'])),stratify=y_rem,shuffle=True,random_state=42)
        return {'X_train': X_train, 'y_train': y_train, 'X_valid': X_valid, 'y_valid': y_valid, 'X_test': X_test, 'y_test': y_test}

splits = train_test_split_stratified(BDT1)

Utils.fit_scaler_to_train(Storage, splits['X_train'][features], root_tag='BDT1')
FIT_branches_w_PCA(Data=splits['X_train'][features],root_tag='BDT1')
X_train1_scaled = Utils.pd_scale(Storage, splits['X_train'][features], root_tag= 'BDT1', target_tag = 'X_train')
X_train1_scaled = TRANSFORM_branches_w_PCA(Data=X_train1_scaled, root_tag='BDT1')
X_test1_scaled = Utils.pd_scale(Storage, splits['X_train'][features], root_tag= 'BDT1', target_tag = 'X_test')
X_test1_scaled = TRANSFORM_branches_w_PCA(Data=X_test1_scaled, root_tag='BDT1')

def Run_tuner(Storage=Storage, root_tag='BDT1'):
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    print('Start with training model 1')
    np.random.seed(42)
    # print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric='log_loss', trials=100, visualization=False, seed=42, verbosity=1, n_jobs=35)

    tuner.fit(X=X_train1_scaled, y=splits['y_train'].astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df=tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state', 'intermediate_values'), multi_index=True), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df=pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0: 'value'}), overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model", tuner=tuner, feature_names=list(X_train1_scaled.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df=pd.DataFrame([tuner.best_params]).T, overwrite=True)

    print('Done with training model')

Run_tuner()

def Verstack_Optimization_plots(root_tag:str=None):
    P = plotting.Verstack_train_plots()
    fig = plt.figure(figsize=(11,3),dpi=300)
    P.plot_optimization_history(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=131)
    P.plot_param_imp(Storage.read_pd(key=f"{root_tag}/tuner_param_imp"),fig=fig,placement=132)
    P.plot_intermediate(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=133)
    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT1_optimization",bbox_inches = 'tight',dpi=300)
    plt.show()

Verstack_Optimization_plots(root_tag='BDT1')

Storage.read_pd(key=f"{'BDT1'}/best_params")

def Predict(Storage, X_target=None, root_tag: str = 'BDT1'):
    print("predict start")
    print(X_target)
    X = X_target.copy()
    print(X[features])

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    #X=X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag='BDT1')

    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:,2], BDT_scores[:,1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)

    return X_target

X_test_BDT1 = Predict(Storage, splits['X_test'], root_tag='BDT1')


def BDT1_response_in_USR__NN1TRB_testset():
    P = plotting.plot_response(input_data=X_test_BDT1, threshold=0.1, root_tag='BDT1', test_data=True)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.95, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT1 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT1_response_in_USR__NN1TRB_testset()

def BDT1_2Dresponse_in_USR__NN1TRB_testset():
    P = plotting.TwoD_response_curves(input_data=X_test_BDT1, threshold=0.4, root_tag='BDT1', test_data=True)

    fig = plt.figure(figsize=(9, 3.5), dpi=300)
    fig.suptitle('GBDT1 2D-Response in $\{\mathrm{SR}^{\mathrm{MC}}\}$ test set, $q_{low}^2$ | Global Selection Rules Applied')
    P.plot_Bd(fig=fig, placemenent=121)
    P.plot_BdBar(fig=fig, placemenent=122)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_2D_response_test", bbox_inches='tight', dpi=300)
    plt.show()

BDT1_2Dresponse_in_USR__NN1TRB_testset()

class feature_importance:
    def __init__(self, X_Train_data: pd.DataFrame, y_Train_data: pd.DataFrame, X_Test_data: pd.DataFrame,
                 y_Test_data: pd.DataFrame, root_tag: str):
        self.X_Train = X_Train_data.copy()
        self.X_Test = X_Test_data.copy()
        self.root_tag = root_tag

        self.y_Train = y_Train_data.copy()
        self.y_Test = y_Test_data.copy()

        self.lgbm_model = None
        self.features = None
        self._load_lightgbm_model_and_set_features()
        self.rets = pd.DataFrame(index=self.features)

    #######################################################################
    class LGBWrapper(BaseEstimator):
        def __init__(self, booster, y):
            self.booster_ = booster
            lb = LabelBinarizer()
            self.y_multiclass = lb.fit_transform(y)

        def fit(self, X, y=None):
            return self

        def predict(self, X):
            return self.booster_.predict(X)

        def predict_proba(self, X):
            return self.booster_.predict(X, raw_score=True)

        def score(self, X, y):
            y_pred = self.predict_proba(X)
            return metrics.roc_auc_score(self.y_multiclass, y_pred, multi_class='ovr')

    #######################################################################

    def _load_lightgbm_model_and_set_features(self) -> None:
        self.lgbm_model = Storage.load_lgbm(key=f"{self.root_tag}/model")
        self.features = list(self.lgbm_model.feature_name())

    def SHAP(self, label: str, max_samples: float = 1.0) -> None:
        assert label == 'Train' or label == 'Test', 'label needs to be either train or test'
        print(f'Init SHAP importances ({label})...')

        if label == 'Train':
            X = self.X_Train
            y = self.y_Train
        elif label == 'Test':
            X = self.X_Test
            y = self.y_Test

        X = X.reset_index()
        y = y.reset_index(drop=True)

        X = X.sample(frac=max_samples, replace=False, random_state=42)
        y = y.iloc[X.index.tolist()]

        X_scaled = Utils.pd_scale(Storage, X[features], root_tag=self.root_tag)
        X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag='BDT1')
        X_scaled = X_scaled[self.features]

        lgbm_shap = self.lgbm_model.predict(X_scaled.reindex(columns=self.features), pred_contrib=True)

        _SHAP_values = np.mean(np.abs(lgbm_shap.reshape((len(X), len(np.unique(y)), len(self.features) + 1)))[:, :, :-1], axis=(0, 1))

        df = pd.DataFrame(columns=['SHAP_' + label], index=self.features, data=_SHAP_values)
        self.rets = pd.concat([self.rets, df], axis=1)
        print('Done with SHAP importances')

    def Native_lgbm(self) -> None:
        print(f'Init Native LGBM importances ...')

        native_importance = self.lgbm_model.feature_importance()
        native_indices = np.argsort(native_importance)
        # Native_importance = pd.DataFrame({'features': np.array(self.features)[native_indices][::-1], 'native_values': native_importance[native_indices][::-1]})

        df = pd.DataFrame(columns=['Native'], index=self.features, data=native_importance)
        self.rets = pd.concat([self.rets, df], axis=1)
        print('Done with Native LGBM importances')

    def Permutation(self, label: str, N_repeats: int = 10, n_jobs=20, max_samples: float = 1.0) -> None:
        assert label == 'Train' or label == 'Test', 'label needs to be either train or test'
        print(f'Init Permutation importances ({label})...')

        if label == 'Train':
            X = self.X_Train
            y = self.y_Train
        elif label == 'Test':
            X = self.X_Test
            y = self.y_Test

        X = X.reset_index()
        y = y.reset_index(drop=True)

        X = X.sample(frac=max_samples, replace=False, random_state=42)
        y = y.iloc[X.index.tolist()]

        X_scaled = Utils.pd_scale(Storage, X[features], root_tag=self.root_tag)
        X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag='BDT1')
        X_scaled = X_scaled[self.features]

        model = self.LGBWrapper(y=y, booster=self.lgbm_model).fit(X_scaled, y)

        PERM_score = sklearn.inspection.permutation_importance(model, X_scaled, y, n_repeats=N_repeats, random_state=0,
                                                               n_jobs=n_jobs)

        df = pd.DataFrame(columns=['perm_' + label], index=self.features, data=PERM_score.importances_mean)
        self.rets = pd.concat([self.rets, df], axis=1)
        print('Done with Permutation importances.')

    def get_Frame(self) -> pd.DataFrame:
        return self.rets

class Feature_Importance_plots():
    def __init__(self, show_N: int = None):
        self.N = show_N

    def plot_2Importance(self, fig, placemenent: int, importance_data1=None, importance_data2=None, label1: str = None, label2: str = None, title: str = 'SHAP Feature Importance', xlabel: str = 'mean(|SHAP| value)\n(average impact on model output magnitude)'):

        ax_shap = fig.add_subplot(placemenent)

        P1 = importance_data1
        if (importance_data2).all() == None:
            P2 = P1
        else:
            P2 = importance_data2.reindex(P1.index)

        if self.N != None:
            P1 = P1.iloc[len(P1) - self.N:]
            P2 = P2.iloc[len(P2) - self.N:]

        x = np.arange(len(P1))
        width = 0.4

        ax_shap.barh(x + width / 2, P1.values, width, label=label1, alpha=1)
        if (importance_data2 != None).all():
            ax_shap.barh(x - width / 2, P2.values, width, label=label2, alpha=1)
        ax_shap.set(title=title, xlabel=xlabel)
        ax_shap.set(yticks=x, yticklabels=list(P1.index), ylim=[2 * width - 1 - width, len(P1)])
        ax_shap.legend(loc='lower right', framealpha=0.95)
        ax_shap.tick_params(axis='y', which='major', labelsize=10)

    def plot_1Importance(self, fig=None, placemenent=133, importance_data=None, title: str = 'LightGBM Feature Importance', xlabel: str = 'Impact on model (Native lightGBM)'):
        ax_native = fig.add_subplot(placemenent)
        native = importance_data

        if self.N != None:
            native = native.iloc[len(native) - self.N:]

        x = np.arange(len(native))
        width = 0.4
        ax_native.barh(x, native.values)
        ax_native.set(yticks=x, yticklabels=list(native.index), ylim=[2 * width - 1 - width, len(native)])
        ax_native.set(title=title, xlabel=xlabel)
        ax_native.tick_params(axis='y', which='major', labelsize=10)

get_importance = feature_importance(X_Train_data=splits['X_train'], y_Train_data=splits['y_train'].astype(int).squeeze() ,X_Test_data=splits['X_test'], y_Test_data=splits['y_test'].astype(int).squeeze(), root_tag='BDT1')
get_importance.Permutation(label='Test',N_repeats=5,n_jobs=15,max_samples=0.05)
get_importance.Permutation(label='Train',N_repeats=5,n_jobs=15,max_samples=0.05)
get_importance.SHAP(label='Test',max_samples=0.05)
get_importance.SHAP(label='Train',max_samples=0.05)
get_importance.Native_lgbm()
Storage.write_pd(key='BDT1_Feature_Importance', df=get_importance.get_Frame(), overwrite=True)

def feature_importance_plots():
    feature_imp_pd = Storage.read_pd(key='BDT1_Feature_Importance')

    # feature_imp_pd.perm_Test= -1*feature_imp_pd.perm_Test
    # feature_imp_pd.perm_Train= -1*feature_imp_pd.perm_Train

    feature_imp_pd_sum = feature_imp_pd.copy()

    scaler = MinMaxScaler()
    feature_imp_pd_sum['SHAP_Test'] = scaler.fit_transform(feature_imp_pd[['SHAP_Test']])
    feature_imp_pd_sum['SHAP_Train'] = scaler.fit_transform(feature_imp_pd[['SHAP_Train']])

    feature_imp_pd_sum['perm_Train'] = scaler.fit_transform(feature_imp_pd[['perm_Train']])
    feature_imp_pd_sum['perm_Test'] = scaler.fit_transform(feature_imp_pd[['perm_Test']])

    feature_imp_pd_sum['Native'] = scaler.fit_transform(feature_imp_pd[['Native']])
    feature_imp_pd_sum['SUM'] = feature_imp_pd_sum[['SHAP_Test', 'perm_Test', 'Native']].sum(1)

    P = Feature_Importance_plots(show_N=20)

    fig = plt.figure(figsize=(15, 6), dpi=300)

    P.plot_2Importance(fig=fig, placemenent=141, importance_data1=feature_imp_pd.SHAP_Test.sort_values(), importance_data2=feature_imp_pd.SHAP_Train.sort_values(), label1='Test', label2='Train', title='SHAP Importance' + '\n' + 'in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$')
    P.plot_2Importance(fig=fig, placemenent=142, importance_data1=feature_imp_pd.perm_Test.sort_values(), importance_data2=feature_imp_pd.perm_Train.sort_values(), label1='Test', label2='Train', title='Permutation Importance' + '\n' + 'in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$', xlabel='Impact on AUC score')
    P.plot_1Importance(fig=fig, placemenent=143, importance_data=feature_imp_pd.Native.sort_values(), title='Native LGBM \nFeature Importance', xlabel='Leaf Split')
    P.plot_1Importance(fig=fig, placemenent=144, importance_data=feature_imp_pd_sum.SUM.sort_values(), title='Sum of SHAP, Perm \nand LGBM', xlabel='Min/Max-scaled Sum')

    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + f"_{'mass_corr_reg_SIG_Features'}_Feature_importances.png", dpi=300, bbox_inches='tight')
    plt.show()

    return feature_imp_pd_sum.SUM.sort_values()[-40:]

_sum = feature_importance_plots()

# Training GBDT2 -------------------------------------------------------------------------------------------------------

BDT2 = pd.concat([USR,SR])

splits_BDT2 = train_test_split_stratified(BDT2)

Utils.fit_scaler_to_train(Storage,splits_BDT2['X_train'][features],root_tag='BDT2')
FIT_branches_w_PCA(Data=splits_BDT2['X_train'][features],root_tag='BDT2')
X_train2_scaled = Utils.pd_scale(Storage,splits_BDT2['X_train'][features], root_tag= 'BDT2', target_tag = 'X_train')
X_train2_scaled = TRANSFORM_branches_w_PCA(Data=X_train2_scaled, root_tag='BDT2')
#X_valid2_scaled = pd_scale(Storage,splits_BDT2['X_valid'][features], root_tag= 'BDT1', target_tag = 'X_valid')
X_test2_scaled = Utils.pd_scale(Storage,splits_BDT2['X_test'][features], root_tag= 'BDT2', target_tag = 'X_test')
X_test2_scaled = TRANSFORM_branches_w_PCA(Data=X_test2_scaled, root_tag='BDT2')

def Run_tuner(Storage=Storage, root_tag='BDT2'):
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    print('Start with training model 2')
    np.random.seed(42)
    # print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric='log_loss', trials=100, visualization=False, seed=42, verbosity=1, n_jobs=35)

    tuner.fit(X=X_train2_scaled, y=splits_BDT2['y_train'].astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df=tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state', 'intermediate_values'), multi_index=True), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df=pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0: 'value'}), overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model", tuner=tuner, feature_names=list(X_train2_scaled.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df=pd.DataFrame([tuner.best_params]).T, overwrite=True)

    print('Done with training model')

    return tuner

BDT2_tuner = Run_tuner()

def Verstack_Optimization_plots(root_tag:str=None):
    P = plotting.Verstack_train_plots()
    fig = plt.figure(figsize=(11,3),dpi=300)
    P.plot_optimization_history(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=131)
    P.plot_param_imp(Storage.read_pd(key=f"{root_tag}/tuner_param_imp"),fig=fig,placement=132)
    P.plot_intermediate(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=133)
    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT2_optimization",bbox_inches = 'tight',dpi=300)
    plt.show()

Verstack_Optimization_plots(root_tag='BDT2')

Storage.read_pd(key=f"{'BDT2'}/best_params")

def Predict(Storage, X_target=None, root_tag: str = 'BDT2'):
    X = X_target.copy()

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag='BDT1')

    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:,2], BDT_scores[:, 1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)

    return X_target

X_test_BDT2 = Predict(Storage, splits_BDT2['X_test'], root_tag='BDT2')

def BDT2_response_in_USR__NN2TRB_testset():
    P = plotting.plot_response(input_data=X_test_BDT2, threshold=0.3, root_tag='BDT2', test_data=True)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.975, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT2 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_response_in_USR__NN2TRB_testset()

def BDT2_2Dresponse_in_USR__NN2TRB_testset():
    P = plotting.TwoD_response_curves(input_data=X_test_BDT2, threshold=0.4, root_tag='BDT2', test_data=True)

    fig = plt.figure(figsize=(9, 3.5), dpi=300)
    fig.suptitle(
        'GBDT2 2D-Response in $\{\mathrm{SR}^{\mathrm{MC}} \}$ test set, $q_{low}^2$ | Global Selection Rules Applied')
    P.plot_Bd(fig=fig, placemenent=121)
    P.plot_BdBar(fig=fig, placemenent=122)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_2D_response_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_2Dresponse_in_USR__NN2TRB_testset()

get_importance = feature_importance(X_Train_data=splits_BDT2['X_train'], y_Train_data=splits_BDT2['y_train'].astype(int).squeeze() , X_Test_data=splits_BDT2['X_test'], y_Test_data=splits_BDT2['y_test'].astype(int).squeeze(), root_tag='BDT2')
get_importance.Permutation(label='Test',N_repeats=5,n_jobs=15,max_samples=0.05)
get_importance.Permutation(label='Train',N_repeats=5,n_jobs=15,max_samples=0.05)
get_importance.SHAP(label='Test',max_samples=0.05)
get_importance.SHAP(label='Train',max_samples=0.05)
get_importance.Native_lgbm()
Storage.write_pd(key='BDT2_Feature_Importance', df=get_importance.get_Frame(), overwrite=True)

def feature_importance_plots():
    feature_imp_pd = Storage.read_pd(key='BDT2_Feature_Importance')

    # feature_imp_pd.perm_Test= -1*feature_imp_pd.perm_Test
    # feature_imp_pd.perm_Train= -1*feature_imp_pd.perm_Train

    feature_imp_pd_sum = feature_imp_pd.copy()

    scaler = MinMaxScaler()
    feature_imp_pd_sum['SHAP_Test'] = scaler.fit_transform(feature_imp_pd[['SHAP_Test']])
    feature_imp_pd_sum['SHAP_Train'] = scaler.fit_transform(feature_imp_pd[['SHAP_Train']])

    feature_imp_pd_sum['perm_Train'] = scaler.fit_transform(feature_imp_pd[['perm_Train']])
    feature_imp_pd_sum['perm_Test'] = scaler.fit_transform(feature_imp_pd[['perm_Test']])

    feature_imp_pd_sum['Native'] = scaler.fit_transform(feature_imp_pd[['Native']])
    feature_imp_pd_sum['SUM'] = feature_imp_pd_sum[['SHAP_Test', 'perm_Test', 'Native']].sum(1)

    P = Feature_Importance_plots(show_N=20)

    fig = plt.figure(figsize=(15, 6), dpi=300)

    P.plot_2Importance(fig=fig, placemenent=141, importance_data1=feature_imp_pd.SHAP_Test.sort_values(), importance_data2=feature_imp_pd.SHAP_Train.sort_values(), label1='Test', label2='Train', title='SHAP Importance' + '\n' + 'in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$')
    P.plot_2Importance(fig=fig, placemenent=142, importance_data1=feature_imp_pd.perm_Test.sort_values(), importance_data2=feature_imp_pd.perm_Train.sort_values(), label1='Test', label2='Train', title='Permutation Importance' + '\n' + 'in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$', xlabel='Impact on AUC score')
    P.plot_1Importance(fig=fig, placemenent=143, importance_data=feature_imp_pd.Native.sort_values(), title='Native LGBM \nFeature Importance', xlabel='Leaf Split')
    P.plot_1Importance(fig=fig, placemenent=144, importance_data=feature_imp_pd_sum.SUM.sort_values(), title='Sum of SHAP, Perm \nand LGBM', xlabel='Min/Max-scaled Sum')

    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + f"_{'mass_corr_reg_SRSB2_Features'}_Feature_importances.png", dpi=300, bbox_inches='tight')
    plt.show()

    return feature_imp_pd_sum.SUM.sort_values()[-40:]

_sum = feature_importance_plots()

# Backup the storage hdf5 file (WIP: Fix the instability of hdf5 storage problem) --------------------------------------
# Current Solution: Backup the hdf5 file after model training + Work at afs instead of eos------------------------------

src_file = config['PATHS_{}'.format(channel)]['Storage']
dst_file = config['PATHS_{}'.format(channel)]['Storage_backup']

with open(src_file, 'rb') as src, open(dst_file, 'wb') as dst:
    dst.write(src.read())

print('LightGBM training finished and saved successfully')