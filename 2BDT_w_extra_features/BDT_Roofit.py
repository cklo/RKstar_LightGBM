import mplhep as hep

# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import toml
from sklearn import preprocessing, model_selection, metrics
import seaborn as sns
import random


import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit, erf
import os

import optuna
random.seed(42)
from iminuit import Minuit
from scipy import stats
mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

import tables
import h5py

import scipy.integrate as integrate
import numpy as np
import ROOT
from array import array
from ROOT import RooFitResult
import pickle
from statistics import mean

# Loading config -------------------------------------------------------------------------------------------------------
config = toml.load('config.toml')
channel = config['channel']['channel']
feather_input = config['BDT_Roofit']['feather_input']
feather_threhold = config['BDT_Roofit']['feather_threhold']
fitting_range = config['BDT_Roofit']['fitting_range']
bin_number = config['BDT_Roofit']['bin_number']
fit_pdf = config['BDT_Roofit']['fit_pdf']
#-----------------------------------------------------------------------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

#-----------------------Accessing Root files---------------------------------
def createCanvasPads():
    c1 =  ROOT.TCanvas('B_Vertex_Mass','Events', 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0) #xmin, ymin, xmax, ymax
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c1.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("B_Vertex_Mass", "B_Vertex_Mass", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c1, pad1, pad2
c1, pad1, pad2 = createCanvasPads()

def SidebandPlotter(channel, feather_input, feather_threhold, fitting_range, bin_number, fit_pdf):
    #-------------------------defining ROOT variables--------------------
    tree = pd.read_feather(f'{feather_input}_{channel}_threshold.ftr')

    attr_names = f"Threshold_{feather_threhold[0]},{feather_threhold[1]}"

    mass = np.zeros(1,dtype=float)

    if channel == "electron":
        LC1, LC2, HC1, HC2 = 3500, 4000, 5700, 6500
    elif channel == "muon":
        LC1, LC2, HC1, HC2 = 4700, 5000, 5700, 6000

    if fitting_range == 'full' or fitting_range == 'sideband':
        LC, HC = LC1, HC2
    elif fitting_range == 'signal':
        LC, HC = LC2, HC1
    else:
        print(f'fitting range in {fitting_range} not supported yet, please check your fitting range again')
        quit()

    w = ROOT.RooWorkspace("w")
    m = ROOT.RooRealVar('m', 'm', LC, HC)
    m.setBins(bin_number)

    m_arg = ROOT.RooArgSet(m)
    w.Import(m_arg)
    data = ROOT.RooDataSet("data", "data", m_arg)

    data.Print("v")
    #-------------------------Accessing data-----------------
    n_events = len(tree)
    print('ALL Events in feather: '+str(n_events))

    counter = 0
    for i in range(n_events):
        mass = tree[attr_names].values[i]
        if (fitting_range == 'sideband' and (LC1 < mass< LC2 or HC1 < mass< HC2)) or ((fitting_range == 'full' or fitting_range == 'signal') and LC < mass < HC):
            ROOT.RooAbsRealLValue.__assign__(m, mass)
            data.add(m_arg,1.0)
            counter += 1
    print('Events satisfy selection requirement: '+str(counter))
    data.Print("v")

    # -----------------Fitting---------------------
    if fitting_range == 'sideband':
        m = w.var('m')
        m.setRange("LSB", LC1, LC2)
        m.setRange("HSB", HC1, HC2)

        # WIP: Adding Roofit function ----------------------------------------------------------------------------------
        if fit_pdf == 'Exp':
            w.factory("RooExponential:model(m, c1[-35e-5, -1e-1, 1e-6])")
        elif fit_pdf == 'Exp_Bukin':
            w.factory("RooExponential:Exp_pdf(m, c1[-35e-5, -1e-1, 1e-6])")
            w.factory("RooBukinPdf:Bukin_pdf(m, Xp[5500,5400,5600], sigp[100,30,150],xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-10,10])")
            w.factory("SUM:model(No_Exp[15000,0,500000]*Exp_pdf, No_Bukin[5000,0,500000]*Bukin_pdf)")
        else:
            print(f'{fit_pdf} currently not available in {fitting_range} fit, please add your fit in BDT_Roofit.py first :)')
            quit()
        # --------------------------------------------------------------------------------------------------------------

        pdf = w.pdf('model')
        pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1),  ROOT.RooFit.Range("LSB,HSB"))
        normSet = ROOT.RooFit.NormSet(m_arg)
        m.setRange("signal", LC2, HC1)
        reg2 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("signal")).getVal()
        reg1 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("LSB")).getVal()
        reg3 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("HSB")).getVal()
        print('Number of events estimated inside sideband region :' +str((reg1+reg3)*counter))
        print('Number of events estimated inside signal region :' +str(reg2*counter))
        #------------------Create pads for plots--------------
        #------------------plotting--------------------
        c1, pad1, pad2 = createCanvasPads()
        pad1.cd()
        pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
        data.plotOn(pl)
        pdf.plotOn(pl, ROOT.RooFit.Range(LC1, LC2), ROOT.RooFit.LineStyle(1))
        pdf.plotOn(pl, ROOT.RooFit.Range(HC1, HC2), ROOT.RooFit.LineStyle(1))
        pdf.plotOn(pl, ROOT.RooFit.Range(LC2, HC1), ROOT.RooFit.LineStyle(2))
        pdf.paramOn(pl, ROOT.RooFit.Layout(0.4,0.6,0.9))
        pdf.plotOn(pl)
        pl.GetXaxis().SetRangeUser(LC, HC)
        pl.SetMinimum(0.01)
        pl.SetTitle("")
        pl.Draw()  #draw on the current pad
        pad2.cd()  #lowerpad

        pullhist = pl.pullHist()
        pl2 = m.frame(ROOT.RooFit.Title(""))
        pl2.addPlotable(pullhist, "P")

        pl2.GetXaxis().SetNdivisions(207)
        pl2.GetYaxis().SetNdivisions(207)
        pl2.GetYaxis().SetLabelSize(0.07)

        pl2.GetYaxis().SetRangeUser(-5., 5.)
        pl2.GetXaxis().SetTitleSize(0.10)
        pl2.GetXaxis().SetLabelSize(0.07)

        pl2.SetTitle("")
        pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
        pl2.GetYaxis().SetTitle("Pull")
        pl2.GetYaxis().SetTitleSize(0.09)
        pl2.GetYaxis().SetTitleOffset(0.3)

        pl2.Draw()

        line = ROOT.TLine(LC,0,HC,0)
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(2)
        line.Draw("same")

        c1.Draw()
        c1.SaveAs(f'Roofit_{fitting_range}_{channel}_{feather_input}.png')

    elif fitting_range == 'full' or fitting_range == 'signal':
        m = w.var('m')
        m.setRange("Overall", LC, HC)

        # WIP: Adding Roofit function ----------------------------------------------------------------------------------
        if fit_pdf == 'Exp':
            w.factory("RooExponential:model(m, c1[-2e-4, -1e-2, 1e-10])")
        elif fit_pdf == 'DSCB':
            w.factory("RooCrystalBall::model(m, mu[5280,5200,5300], width1[71.7,50,90], width2[65.699,50,90], a1[0.9256,0.5,2], p1[7.71,1,150], a2[0.700028,0.5,2], p2[4.190,1,150])")
        elif fit_pdf == 'Bukin':
            w.factory("RooBukinPdf:model(m, Xp[5500,5400,5600], sigp[100,30,150],xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-10,10])")
        elif fit_pdf == 'Exp_Bukin':
            w.factory("RooExponential:Exp_pdf(m, c1[-35e-5, -1e-1, 1e-6])")
            w.factory("RooBukinPdf:Bukin_pdf(m, Xp[5500,5400,5600], sigp[100,30,150],xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-10,10])")
            w.factory("SUM:model(No_Exp[15000,0,500000]*Exp_pdf, No_Bukin[5000,0,500000]*Bukin_pdf)")
        elif fit_pdf == 'Exp_DSCB':
            w.factory("RooCrystalBall::DSCB_pdf(m, mu[5280,5200,5300], width1[71.7,50,90], width2[65.699,50,90], a1[0.9256,0.5,2], p1[7.71,1,150], a2[0.700028,0.5,2], p2[4.190,1,150])")
            w.factory("RooExponential:Exp_pdf(m, c1[-2e-4, -1e-2, 1e-10])")
            w.factory("SUM:model(No_sig[15000,0,500000]*DSCB_pdf, No_bkg[5000,0,500000]*Exp_pdf)")
        elif fit_pdf == 'Exp_Bukin_DSCB':
            w.factory("RooCrystalBall::DSCB_pdf(m, mu[5280,5200,5300], width1[71.7,50,90], width2[65.699,50,90], a1[0.9256,0.5,2], p1[7.71,1,150], a2[0.700028,0.5,2], p2[4.190,1,150])")
            w.factory("RooExponential:Exp_pdf(m, c1[-2e-4, -1e-2, 1e-10])")
            w.factory("RooBukinPdf:Bukin_pdf(m, Xp[5500,5400,5600], sigp[100,30,150],xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-10,10])")
            w.factory("SUM:bkg_pdf(No_Exp[800,0,500000]*Exp_pdf, No_Bukin[800,0,500000]*Bukin_pdf)")
            w.factory("SUM:model(No_sig[15000,0,500000]*DSCB_pdf, No_bkg[5000,0,500000]*bkg_pdf)")
        else:
            print(f'{fit_pdf} currently not available in {fitting_range} fit, please add your fit in BDT_Roofit.py first :)')
            quit()
        # --------------------------------------------------------------------------------------------------------------

        pdf = w.pdf('model')
        pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.Range("Overall"))
        # ------------------Create pads for plots--------------
        # ------------------plotting--------------------
        c1, pad1, pad2 = createCanvasPads()
        pad1.cd()  # upper pad
        pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
        data.plotOn(pl)
        pdf.plotOn(pl, ROOT.RooFit.Components("model"), ROOT.RooFit.LineColor(3), ROOT.RooFit.LineStyle(2))

        pdf.paramOn(pl, ROOT.RooFit.Layout(0.7, 0.9, 0.9))
        pdf.plotOn(pl)

        legend = ROOT.TLegend(0.15, 0.15, 0.25, 0.25)  # Position (xmin,ymin,xmax,ymax)
        legend.SetBorderSize(0)  # no border
        legend.SetFillStyle(0)  # transparent
        legend.SetTextSize(.05)

        pl.GetXaxis().SetRangeUser(LC, HC)
        # pl.GetYaxis().SetRangeUser(0., 1500.)
        pl.SetMinimum(0.01)
        # pl.GetYaxis().SetRangeUser(0.01, 1000.)
        pl.SetTitle("")
        pl.Draw()  # draw on the current pad
        legend.Draw()
        pad2.cd()  # lowerpad

        pullhist = pl.pullHist()
        pl2 = m.frame(ROOT.RooFit.Title(""))
        pl2.addPlotable(pullhist, "P")

        pl2.GetXaxis().SetNdivisions(207)
        pl2.GetYaxis().SetNdivisions(207)
        pl2.GetYaxis().SetLabelSize(0.07)

        pl2.GetYaxis().SetRangeUser(-5., 5.)
        pl2.GetXaxis().SetTitleSize(0.10)
        pl2.GetXaxis().SetLabelSize(0.07)

        pl2.SetTitle("")
        pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
        pl2.GetYaxis().SetTitle("Pull")
        pl2.GetYaxis().SetTitleSize(0.09)
        pl2.GetYaxis().SetTitleOffset(0.3)

        pl2.Draw()

        line = ROOT.TLine(LC, 0, HC, 0) # LC2, 0, HC1, 0
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(2)
        line.Draw("same")

        c1.Draw()
        c1.SaveAs(f'Roofit_{fitting_range}_{channel}_{feather_input}.png')
    print("Roofit completed and saved successfully")

SidebandPlotter(channel, feather_input, feather_threhold, fitting_range, bin_number, fit_pdf)